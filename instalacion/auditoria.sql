drop table MIS."auditoria";

create TABLE MIS."auditoria"
(
    "esquema"    varchar2(255),
    "tabla"      varchar2(255),
    "dbusuario"  varchar2(255),
    "fecha_hora" TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
    "accion"     varchar2(1),
    "original"   varchar2(255),
    "nuevo"      varchar2(255)
) TABLESPACE TS_BH_LST;


create or replace trigger TR_USUARIOS
    before update
    on MIS."usuarios"
    for each row
begin

    INSERT INTO MIS."auditoria" ("esquema", "tabla", "dbusuario", "fecha_hora", "accion", "original", "nuevo")
    VALUES ('MIS', 'usuarios', user, CURRENT_DATE, 'U',
            '{ ' || 'coddb: ' || :old."codbh" || ' , ' || 'usuario: ' || :old."usuario" || ' }',
            '{ ' || 'coddb: ' || :new."codbh" || ' , ' || 'usuario: ' || :new."usuario" || ' }');
end TR_USUARIOS;

create or replace trigger TR_AREAS
    before update
    on MIS."areas"
    for each row
begin

    INSERT INTO MIS."auditoria" ("esquema", "tabla", "dbusuario", "fecha_hora", "accion", "original", "nuevo")
    VALUES ('MIS', 'areas', user, CURRENT_DATE, 'U',
            '{ ' || 'id: ' || :old."id" || ' , ' || 'nombre: ' || :old."nombre" || ' , ' || 'estado: ' ||
            :old."estado" || ' , ' ||
            'createdAt: ' || :old."createdAt" || ' , ' || 'createdBy: ' || :old."createdBy" ||
            'updatedAt: ' || :old."updatedAt" || ' , ' || 'updatedBy: ' || :old."updatedBy" ||
            '  }',
            '{ ' || 'id: ' || :new."id" || ' , ' || 'nombre: ' || :new."nombre" || ' , ' || 'estado: ' ||
            :new."estado" || ' , ' ||
            'createdAt: ' || :new."createdAt" || ' , ' || 'createdBy: ' || :new."createdBy" ||
            'updatedAt: ' || :new."updatedAt" || ' , ' || 'updatedBy: ' || :new."updatedBy" ||
             '  }');
end TR_AREAS;

create or replace trigger TR_CAMPOS
    before update
    on MIS."campos"
    for each row
begin

    INSERT INTO MIS."auditoria" ("esquema", "tabla", "dbusuario", "fecha_hora", "accion", "original", "nuevo")
    VALUES ('MIS', 'campos', user, CURRENT_DATE, 'U',
            '{ ' || 'id: ' || :old."id" || ' , ' || 'nombre: ' || :old."nombre" || ' , ' || 'label: ' || :old."label" ||
            ' , ' || 'default: ' || :old."default" || ' , ' || 'tipo: ' || :old."tipo" || ' , ' || 'formato: ' ||
            :old."formato" || ' , ' || 'descripcion: ' || :old."descripcion" ||
            'createdAt: ' || :old."createdAt" || ' , ' || 'createdBy: ' || :old."createdBy" ||
            'updatedAt: ' || :old."updatedAt" || ' , ' || 'updatedBy: ' || :old."updatedBy" ||
            '  }',
            '{ ' || 'id: ' || :new."id" || ' , ' || 'nombre: ' || :new."nombre" || ' , ' || 'label: ' || :new."label" ||
            ' , ' || 'default: ' || :new."default" || ' , ' || 'tipo: ' || :new."tipo" || ' , ' || 'formato: ' ||
            :new."formato" || ' , ' || 'descripcion: ' || :new."descripcion" ||
            'createdAt: ' || :new."createdAt" || ' , ' || 'createdBy: ' || :new."createdBy" ||
            'updatedAt: ' || :new."updatedAt" || ' , ' || 'updatedBy: ' || :new."updatedBy" ||
            '  }');
end TR_CAMPOS;

create or replace trigger TR_CAMPOS_SUBCATEGORIAS
    before update
    on MIS."campos_subcategorias"
    for each row
begin

    INSERT INTO MIS."auditoria" ("esquema", "tabla", "dbusuario", "fecha_hora", "accion", "original", "nuevo")
    VALUES ('MIS', 'campos_subcategorias', user, CURRENT_DATE, 'U',
            '{ ' || 'id: ' || :old."id" || ' , ' || 'label: ' || :old."label" || ' , ' || 'valor: ' || :old."valor" ||
            'createdAt: ' || :old."createdAt" || ' , ' || 'createdBy: ' || :old."createdBy" ||
            'updatedAt: ' || :old."updatedAt" || ' , ' || 'updatedBy: ' || :old."updatedBy" ||
            '  }',
            '{ ' || 'id: ' || :new."id" || ' , ' || 'label: ' || :new."label" || ' , ' || 'valor: ' || :new."valor" ||
            'createdAt: ' || :new."createdAt" || ' , ' || 'createdBy: ' || :new."createdBy" ||
            'updatedAt: ' || :new."updatedAt" || ' , ' || 'updatedBy: ' || :new."updatedBy" ||
            '  }');
end TR_CAMPOS_SUBCATEGORIAS;


