 SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
--  drop table MIS."campos_subcategorias" purge;
--  drop table MIS."transacciones_detalle" purge;
--  drop table MIS."transacciones" purge;
--  drop table MIS."cargas_masivas" purge;
--  drop table MIS."listas_campos_config" purge;
--  drop table MIS."conexiones_externas" purge;
--  drop table MIS."usuarios_roles" purge;
--  drop table MIS."usuarios_areas" purge;
--  drop table MIS."areas" purge;
--  drop table MIS."campos_perfiles" purge;
--  drop table MIS."perfiles" purge;
--  drop table MIS."roles" purge;
--  drop table MIS."campos" purge;
--  drop table MIS."listas_responsables" purge;
--  drop table MIS."usuarios" purge;
--  drop table MIS."listas" purge;
--  drop table MIS."auditoria" purge;
--
--  drop synonym campos_subcategorias;
--  drop synonym transacciones_detalle;
--  drop synonym transacciones ;
--  drop synonym cargas_masivas ;
--  drop synonym listas_campos_config ;
--  drop synonym conexiones_externas ;
--  drop synonym usuarios_roles ;
--  drop synonym usuarios_areas ;
--  drop synonym areas ;
--  drop synonym campos_perfiles ;
--  drop synonym perfiles ;
--  drop synonym roles ;
--  drop synonym campos ;
--  drop synonym listas_responsables ;
--  drop synonym usuarios ;
--  drop synonym listas ;
--  drop synonym auditoria ;
 commit;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;

create TABLE MIS."auditoria"
(
    "esquema"    varchar2(255),
    "tabla"      varchar2(255),
    "dbusuario"  varchar2(255),
    "fecha_hora" TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
    "accion"     varchar2(1),
    "usuarioapp" varchar2(255),
    "original"   varchar2(255),
    "nuevo"      varchar2(255),
    "query"      varchar2(255)
) TABLESPACE TS_BH_LST;

create index IDX_AUDIFH on MIS."auditoria" ("fecha_hora") TABLESPACE TS_BH_IDX_LST;

comment on column MIS."auditoria"."esquema" is 'Esquema donde se da el evento';
comment on column MIS."auditoria"."tabla" is 'Tabla afectada por el evento';
comment on column MIS."auditoria"."dbusuario" is 'Usuario de base de datos que dispara el evento';
comment on column MIS."auditoria"."fecha_hora" is 'Fecha / Hora del evento';
comment on column MIS."auditoria"."accion" is 'Tipo de Evento (I/D/U)';
comment on column MIS."auditoria"."usuarioapp" is 'Usuario de aplicacion relacionado';
comment on column MIS."auditoria"."original" is 'Registro antes del evento';
comment on column MIS."auditoria"."nuevo" is 'Registro luego del evento';
comment on column MIS."auditoria"."query" is 'Sentencia SQL ejecutada.';

create TABLE MIS."campos_subcategorias"
(
    "id"        varchar2(255),
    "label"     varchar2(255)                       NOT NULL,
    "valor"     varchar2(255)                       NOT NULL,
    "createdAt" TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
	"createdBy" VARCHAR2(10) not null,
	"updatedAt" TIMESTAMP(6) default CURRENT_TIMESTAMP,
	"updatedBy" VARCHAR2(10),
    "version"   number                              NOT NULL,
    "campoId"   varchar2(255),
    CONSTRAINT "PK_campos_subcategorias" PRIMARY KEY ("id")
) TABLESPACE TS_BH_LST;

create UNIQUE INDEX "IDX_campos_subcategorias" ON MIS."campos_subcategorias" ("valor", "campoId") TABLESPACE TS_BH_IDX_LST;

comment on column MIS."campos_subcategorias"."id" is 'Id de la tabla';
comment on column MIS."campos_subcategorias"."label" is 'Nombre de la subcategoria a mostrar en pantalla';
comment on column MIS."campos_subcategorias"."valor" is 'Valor de la subcategoria';
comment on column MIS."campos_subcategorias"."createdAt" is 'Fecha de creación';
comment on column MIS."campos_subcategorias"."createdBy" is 'Usuario que creó el registro';
comment on column MIS."campos_subcategorias"."updatedAt" is 'Fecha de Actualización';
comment on column MIS."campos_subcategorias"."updatedBy" is 'Usuario que actualizó el registro';
comment on column MIS."campos_subcategorias"."version" is 'Version del registro';
comment on column MIS."campos_subcategorias"."campoId" is 'Id del campo al que pertenece la subcategoria';


create TABLE MIS."transacciones_detalle"
(
    "id"            varchar2(255),
    "valor"         varchar2(255) NOT NULL,
    "campoId"       varchar2(255),
    "transaccionId" varchar2(255),
    "createdAt" TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
    "createdBy" VARCHAR2(10) not null,
    "updatedAt" TIMESTAMP(6) default CURRENT_TIMESTAMP,
    "updatedBy" VARCHAR2(10),
    CONSTRAINT "PK_transacciones_detalle" PRIMARY KEY ("id")
) TABLESPACE TS_BH_LST;

comment on column MIS."transacciones_detalle"."id" is 'Id de la tabla';
comment on column MIS."transacciones_detalle"."valor" is 'Valor del campo asociado al registro y a la estructura de lista';
comment on column MIS."transacciones_detalle"."campoId" is 'Id del campo del cual se ha hecho el registro';
comment on column MIS."transacciones_detalle"."transaccionId" is 'Id de la trasancción a la cual pertenece la transacción_detallo (Campo asociado a una lista)';
comment on column MIS."transacciones_detalle"."createdAt" is 'Fecha de creación';
comment on column MIS."transacciones_detalle"."createdBy" is 'Usuario que creó el registro';
comment on column MIS."transacciones_detalle"."updatedAt" is 'Fecha de Actualización';
comment on column MIS."transacciones_detalle"."updatedBy" is 'Usuario que actualizó el registro';


create TABLE MIS."transacciones"
(
    "id"            varchar2(255),
    "createdAt" TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
    "createdBy" VARCHAR2(10) not null,
    "updatedAt" TIMESTAMP(6) default CURRENT_TIMESTAMP,
    "updatedBy" VARCHAR2(10),
    "version"       number                              NOT NULL,
    "cargaMasivaId" varchar2(255),
    "justificacion" VARCHAR2(255),
    "estado" VARCHAR2(1),
    CONSTRAINT "PK_transacciones" PRIMARY KEY ("id")
) TABLESPACE TS_BH_LST;

comment on column MIS."transacciones"."id" is 'Id de la tabla';
comment on column MIS."transacciones"."createdAt" is 'Fecha de creación';
comment on column MIS."transacciones"."createdBy" is 'Usuario que creó el registro';
comment on column MIS."transacciones"."updatedAt" is 'Fecha de Actualización';
comment on column MIS."transacciones"."updatedBy" is 'Usuario que actualizó el registro';
comment on column MIS."transacciones"."version" is 'Version del registro';
comment on column MIS."transacciones"."cargaMasivaId" is 'Id de la carga a la cual pertenece el registro';
comment on column MIS."transacciones"."justificacion" is 'Justificacion al dar de baja';
comment on column MIS."transacciones"."estado" is 'Estado de la transaccion';


create TABLE MIS."cargas_masivas"
(
    "id"        varchar2(255),
    "estado"    varchar2(1)                         NOT NULL,
    "forma"     varchar2(1)                         NOT NULL,
    "motivo"    varchar2(255),
    "createdAt" TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
    "createdBy" VARCHAR2(10) not null,
    "updatedAt" TIMESTAMP(6) default CURRENT_TIMESTAMP,
    "updatedBy" VARCHAR2(10),
    "version"   number                              NOT NULL,
    "areaId"    varchar2(255),
    "usuarioId" varchar2(255),
    "listaId"   varchar2(255),
    CONSTRAINT "PK_cargas_masivas" PRIMARY KEY ("id")
) TABLESPACE TS_BH_LST;

comment on column MIS."cargas_masivas"."id" is 'Id de la tabla';
comment on column MIS."cargas_masivas"."estado" is 'Estado del registro';
comment on column MIS."cargas_masivas"."forma" is 'Forma del registro (Masivo/Individual)';
comment on column MIS."cargas_masivas"."motivo" is 'Motivo por el cual se da de baja';
comment on column MIS."cargas_masivas"."createdAt" is 'Fecha de creación';
comment on column MIS."cargas_masivas"."createdBy" is 'Usuario que creó el registro';
comment on column MIS."cargas_masivas"."updatedAt" is 'Fecha de Actualización';
comment on column MIS."cargas_masivas"."updatedBy" is 'Usuario que actualizó el registro';
comment on column MIS."cargas_masivas"."version" is 'Version del registro';
comment on column MIS."cargas_masivas"."areaId" is 'Id del area que solicita la carga';
comment on column MIS."cargas_masivas"."usuarioId" is 'Id del usurio que realiza la carga';
comment on column MIS."cargas_masivas"."listaId" is 'Id de la lista a la que pretenece la carga';

create TABLE MIS."areas"
(
    "id"        varchar2(255),
    "nombre"    varchar2(50)                        NOT NULL,
    "estado"    varchar2(1)                         NOT NULL,
    "createdAt" TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
    "createdBy" VARCHAR2(10) not null,
    "updatedAt" TIMESTAMP(6) default CURRENT_TIMESTAMP,
    "updatedBy" VARCHAR2(10),
    "version"   number                              NOT NULL,
    CONSTRAINT "PK_areas" PRIMARY KEY ("id")
) TABLESPACE TS_BH_LST;

comment on column MIS."areas"."id" is 'Id de la tabla';
comment on column MIS."areas"."nombre" is 'Nombre del área';
comment on column MIS."areas"."estado" is 'Estado del registro';
comment on column MIS."areas"."createdAt" is 'Fecha de creación';
comment on column MIS."areas"."createdBy" is 'Usuario que creó el registro';
comment on column MIS."areas"."updatedAt" is 'Fecha de Actualización';
comment on column MIS."areas"."updatedBy" is 'Usuario que actualizó el registro';
comment on column MIS."areas"."version" is 'Version del registro';


create TABLE MIS."usuarios" (
    "id"        varchar2(255),
    "codbh"     varchar2(255)                       NOT NULL,
    "usuario"   varchar2(255)                       NOT NULL,
    "createdAt" TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
    "createdBy" VARCHAR2(10) not null,
    "updatedAt" TIMESTAMP(6) default CURRENT_TIMESTAMP,
    "updatedBy" VARCHAR2(10),
    "version"   number                              NOT NULL,
    CONSTRAINT "UQ_usuarios_codbh" UNIQUE ("codbh"),
    CONSTRAINT "PK_usuarios" PRIMARY KEY ("id")
) TABLESPACE TS_BH_LST;

comment on column MIS."usuarios"."id" is 'Id de la tabla';
comment on column MIS."usuarios"."codbh" is 'Codigo interno de Banco hipotecario para usuarios';
comment on column MIS."usuarios"."usuario" is 'Nombre del usuario';
comment on column MIS."usuarios"."createdAt" is 'Fecha de creación';
comment on column MIS."usuarios"."createdBy" is 'Usuario que creó el registro';
comment on column MIS."usuarios"."updatedAt" is 'Fecha de Actualización';
comment on column MIS."usuarios"."updatedBy" is 'Usuario que actualizó el registro';
comment on column MIS."usuarios"."version" is 'Version del registro';


create TABLE MIS."roles"
(
    "id"        varchar2(255),
    "idBhRol"   number                              NOT NULL,
    "nombre"    varchar2(255)                       NOT NULL,
    "createdAt" TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
    "updatedAt" TIMESTAMP(6) default CURRENT_TIMESTAMP,
    "version"   number                              NOT NULL,
    CONSTRAINT "UQ_roles_idBhRol" UNIQUE ("idBhRol"),
    CONSTRAINT "PK_roles" PRIMARY KEY ("id")
) TABLESPACE TS_BH_LST;

comment on column MIS."roles"."id" is 'Id de la tabla';
comment on column MIS."roles"."idBhRol" is 'Codigo interno de Banco hipotecario para roles';
comment on column MIS."roles"."nombre" is 'Nombre del rol';
comment on column MIS."roles"."createdAt" is 'Fecha de creación';
comment on column MIS."roles"."updatedAt" is 'Fecha de Actualización';
comment on column MIS."roles"."version" is 'Version del registro';

create TABLE MIS."perfiles"
(
    "id"          varchar2(255),
    "acceso"      varchar2(10)                        NOT NULL,
    "visualizar"  varchar2(10)                        NOT NULL,
    "descargar"   number                              NOT NULL,
    "responsable" number                              NOT NULL,
    "version"     number                              NOT NULL,
    "createdAt" TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
    "createdBy" VARCHAR2(10) not null,
    "updatedAt" TIMESTAMP(6) default CURRENT_TIMESTAMP,
    "updatedBy" VARCHAR2(10),
    "listaId"     varchar2(255),
    "rolId"       varchar2(255),
    CONSTRAINT "PK_perfiles" PRIMARY KEY ("id")
) TABLESPACE TS_BH_LST;

create UNIQUE INDEX "IDX_perfiles" ON MIS."perfiles" ("listaId", "rolId") TABLESPACE TS_BH_IDX_LST;

comment on column MIS."perfiles"."id" is 'Id de la tabla';
comment on column MIS."perfiles"."acceso" is 'Tipo de acceso que tiene el perfil (Ninguno | Consulta | Edicion)';
comment on column MIS."perfiles"."visualizar" is 'Tipo de visualización que tiene el perfil (Vigente | Historico)';
comment on column MIS."perfiles"."descargar" is 'Si puede descargar';
comment on column MIS."perfiles"."responsable" is 'Si puede ver responsable';
comment on column MIS."perfiles"."createdAt" is 'Fecha de creación';
comment on column MIS."perfiles"."createdBy" is 'Usuario que creó el registro';
comment on column MIS."perfiles"."updatedAt" is 'Fecha de Actualización';
comment on column MIS."perfiles"."updatedBy" is 'Usuario que actualizó el registro';
comment on column MIS."perfiles"."version" is 'Version del registro';
comment on column MIS."perfiles"."listaId" is 'Id de lista a la que se le asignan permisos';
comment on column MIS."perfiles"."rolId" is 'Id del rol al que se le asignan permisos';



create TABLE MIS."campos"
(
    "id"          varchar2(255),
    "nombre"      varchar2(50)                        NOT NULL,
    "label"       varchar2(100)                       NOT NULL,
    "default"     number                              NOT NULL,
    "tipo"        varchar2(10)                        NOT NULL,
    "formato"     varchar2(10)                        NOT NULL,
    "descripcion" varchar2(254)                       NOT NULL,
    "createdAt" TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
    "createdBy" VARCHAR2(10) not null,
    "updatedAt" TIMESTAMP(6) default CURRENT_TIMESTAMP,
    "updatedBy" VARCHAR2(10),
    "version"     number                              NOT NULL,
    CONSTRAINT "PK_campos" PRIMARY KEY ("id")
) TABLESPACE TS_BH_LST;

comment on column MIS."campos"."id" is 'Id de la tabla';
comment on column MIS."campos"."nombre" is 'Nombre del campo';
comment on column MIS."campos"."label" is 'Nombre de la subcategoria a mostrar en pantalla';
comment on column MIS."campos"."default" is 'Si es predeterminado para todas las listas';
comment on column MIS."campos"."tipo" is 'Tipo de campo (selectbox | input)';
comment on column MIS."campos"."formato" is 'Formato de campo (text | num | date | scat)';
comment on column MIS."campos"."descripcion" is 'Descripción del campo';
comment on column MIS."campos"."createdAt" is 'Fecha de creación';
comment on column MIS."campos"."createdBy" is 'Usuario que creó el registro';
comment on column MIS."campos"."updatedAt" is 'Fecha de Actualización';
comment on column MIS."campos"."updatedBy" is 'Usuario que actualizó el registro';
comment on column MIS."campos"."version" is 'Version del registro';

create TABLE MIS."listas_campos_config"
(
    "id"        varchar2(255),
    "requerido" number                              NOT NULL,
    "busqueda"  number                              NOT NULL,
    "orden"     number                              NOT NULL,
    "estado"    varchar2(1)                         NOT NULL,
    "version"   number                              NOT NULL,
    "createdAt" TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
    "createdBy" VARCHAR2(10) not null,
    "updatedAt" TIMESTAMP(6) default CURRENT_TIMESTAMP,
    "updatedBy" VARCHAR2(10),
    "listaId"   varchar2(255),
    "campoId"   varchar2(255),
    CONSTRAINT "PK_listas_campos_config" PRIMARY KEY ("id")
) TABLESPACE TS_BH_LST;

create UNIQUE INDEX "IDX_listas_campos_config" ON MIS."listas_campos_config" ("listaId", "campoId") TABLESPACE TS_BH_IDX_LST;

comment on column MIS."listas_campos_config"."id" is 'Id de la tabla';
comment on column MIS."listas_campos_config"."requerido" is 'Si el campo es requerido';
comment on column MIS."listas_campos_config"."busqueda" is 'Si el campo es de búsqueda';
comment on column MIS."listas_campos_config"."orden" is 'Orden del campos en la lista asociada';
comment on column MIS."listas_campos_config"."estado" is 'Estado del registro';
comment on column MIS."listas_campos_config"."version" is 'Version del registro';
comment on column MIS."listas_campos_config"."createdAt" is 'Fecha de creación';
comment on column MIS."listas_campos_config"."createdBy" is 'Usuario que creó el registro';
comment on column MIS."listas_campos_config"."updatedAt" is 'Fecha de Actualización';
comment on column MIS."listas_campos_config"."updatedBy" is 'Usuario que actualizó el registro';
comment on column MIS."listas_campos_config"."listaId" is 'Id de la lista a la que esta asociada la configuración del campo';
comment on column MIS."listas_campos_config"."campoId" is 'Id del campo al que esta asociada la configuración del campo';

create TABLE MIS."listas"
(
    "id"          varchar2(255),
    "nombre"      varchar2(20)                        NOT NULL,
    "descripcion" varchar2(254),
    "tipo"        varchar2(10)                        NOT NULL,
    "estado"      varchar2(1)                         NOT NULL,
    "criticidad"  varchar2(255)                       NOT NULL,
    "observaciones" varchar2(255),
    "createdAt" TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
    "createdBy" VARCHAR2(10) not null,
    "updatedAt" TIMESTAMP(6) default CURRENT_TIMESTAMP,
    "updatedBy" VARCHAR2(10),
    "version"     number                              NOT NULL,
    CONSTRAINT "UQ_listas" UNIQUE ("nombre") ,
    CONSTRAINT "PK_listas" PRIMARY KEY ("id")
) TABLESPACE TS_BH_LST;

comment on column MIS."listas"."id" is 'Id de la tabla';
comment on column MIS."listas"."nombre" is 'Nombre de la Lista';
comment on column MIS."listas"."descripcion" is 'Descripción de la lista';
comment on column MIS."listas"."tipo" is 'Si es Interna o Externa';
comment on column MIS."listas"."estado" is 'Estado del registro';
comment on column MIS."listas"."criticidad" is 'Mensajes de criticidad';
comment on column MIS."listas"."observaciones" is 'Observaciones al dar de baja una lista';
comment on column MIS."listas"."createdAt" is 'Fecha de creación';
comment on column MIS."listas"."createdBy" is 'Usuario que creó el registro';
comment on column MIS."listas"."updatedAt" is 'Fecha de Actualización';
comment on column MIS."listas"."updatedBy" is 'Usuario que actualizó el registro';
comment on column MIS."listas"."version" is 'Version del registro';

create TABLE MIS."conexiones_externas"
(
    "id"        varchar2(255),
    "url"       varchar2(255)                       NOT NULL,
    "createdAt" TIMESTAMP(6) default CURRENT_TIMESTAMP not null,
    "createdBy" VARCHAR2(10) not null,
    "updatedAt" TIMESTAMP(6) default CURRENT_TIMESTAMP,
    "updatedBy" VARCHAR2(10),
    "version"   number                              NOT NULL,
    "listaId"   varchar2(255),
    CONSTRAINT "REL_conex_ext_listaId" UNIQUE ("listaId"),
    CONSTRAINT "PK_conex_ext" PRIMARY KEY ("id")
) TABLESPACE TS_BH_LST;

comment on column MIS."conexiones_externas"."id" is 'Id de la tabla';
comment on column MIS."conexiones_externas"."url" is 'url de conexión';
comment on column MIS."conexiones_externas"."createdAt" is 'Fecha de creación';
comment on column MIS."conexiones_externas"."createdBy" is 'Usuario que creó el registro';
comment on column MIS."conexiones_externas"."updatedAt" is 'Fecha de Actualización';
comment on column MIS."conexiones_externas"."updatedBy" is 'Usuario que actualizó el registro';
comment on column MIS."conexiones_externas"."version" is 'Version del registro';
comment on column MIS."conexiones_externas"."listaId" is 'Id de la lista a la que se asocia la conexión';


create TABLE MIS."usuarios_roles"
(
    "usuariosId" varchar2(255) NOT NULL,
    "rolesId"    varchar2(255) NOT NULL,
    CONSTRAINT "PK_usuarios_roles" PRIMARY KEY ("usuariosId", "rolesId")
) TABLESPACE TS_BH_LST;

create INDEX "IDX_usuarios_roles_usuariosId" ON MIS."usuarios_roles" ("usuariosId") TABLESPACE TS_BH_IDX_LST;
create INDEX "IDX_usuarios_roles_rolesId" ON MIS."usuarios_roles" ("rolesId") TABLESPACE TS_BH_IDX_LST;

comment on column MIS."usuarios_roles"."usuariosId" is 'Id del usuario asociado';
comment on column MIS."usuarios_roles"."rolesId" is 'Id del rol asociado';

create TABLE MIS."usuarios_areas"
(
    "usuariosId" varchar2(255) NOT NULL,
    "areasId"    varchar2(255) NOT NULL,
    CONSTRAINT "PK_usuarios_areas" PRIMARY KEY ("usuariosId", "areasId")
) TABLESPACE TS_BH_LST;

create INDEX "IDX_usuarios_areas_usuariosId" ON MIS."usuarios_areas" ("usuariosId") TABLESPACE TS_BH_IDX_LST;
create INDEX "IDX_usuarios_areas_areasId" ON MIS."usuarios_areas" ("areasId") TABLESPACE TS_BH_IDX_LST;

comment on column MIS."usuarios_areas"."usuariosId" is 'Id del usuario asociado';
comment on column MIS."usuarios_areas"."areasId" is 'Id del área asociada';

create TABLE MIS."campos_perfiles"
(
    "camposId"   varchar2(255) NOT NULL,
    "perfilesId" varchar2(255) NOT NULL,
    CONSTRAINT "PK_8d8ba91" PRIMARY KEY ("camposId", "perfilesId")
) TABLESPACE TS_BH_LST;

create INDEX "IDX_campos_perfiles_camposId" ON MIS."campos_perfiles" ("camposId") TABLESPACE TS_BH_IDX_LST;
create INDEX "IDX_campos_perfiles_perfilesId" ON MIS."campos_perfiles" ("perfilesId") TABLESPACE TS_BH_IDX_LST;

comment on column MIS."campos_perfiles"."camposId" is 'Id del campo asociado';
comment on column MIS."campos_perfiles"."perfilesId" is 'Id del perfil asociado';

create TABLE MIS."listas_responsables"
(
    "listasId"   varchar2(255) NOT NULL,
    "usuariosId" varchar2(255) NOT NULL,
    CONSTRAINT "PK_listas_responsables" PRIMARY KEY ("listasId", "usuariosId")
) TABLESPACE TS_BH_LST;

create INDEX "IDX_listas_resp_listasId" ON MIS."listas_responsables" ("listasId") TABLESPACE TS_BH_IDX_LST;
create INDEX "IDX_listas_resp_usuariosId" ON MIS."listas_responsables" ("usuariosId") TABLESPACE TS_BH_IDX_LST;

comment on column MIS."listas_responsables"."listasId" is 'Id de la lista asociada';
comment on column MIS."listas_responsables"."usuariosId" is 'Id del usuario asociado';


alter table MIS."campos_subcategorias"
    add CONSTRAINT "FK_campos_subcat_campoId" FOREIGN KEY ("campoId") REFERENCES MIS. "campos" ("id");
alter table MIS."transacciones_detalle"
    add CONSTRAINT "FK_transac_det_campoId" FOREIGN KEY ("campoId") REFERENCES MIS."campos" ("id");
alter table MIS."transacciones_detalle"
    add CONSTRAINT "FK_transac_det_transaccionId" FOREIGN KEY ("transaccionId") REFERENCES MIS."transacciones" ("id");
alter table MIS."transacciones"
    add CONSTRAINT "FK_transac_cargaMasivaId" FOREIGN KEY ("cargaMasivaId") REFERENCES MIS."cargas_masivas" ("id");
alter table MIS."cargas_masivas"
    add CONSTRAINT "FK_cargas_masivas_areaId" FOREIGN KEY ("areaId") REFERENCES MIS."areas" ("id");
alter table MIS."cargas_masivas"
    add CONSTRAINT "FK_cargas_masivas_usuarioId" FOREIGN KEY ("usuarioId") REFERENCES MIS."usuarios" ("id");
alter table MIS."cargas_masivas"
    add CONSTRAINT "FK_cargas_masivas_listaId" FOREIGN KEY ("listaId") REFERENCES MIS."listas" ("id");
alter table MIS."perfiles"
    add CONSTRAINT "FK_perfiles_listaId" FOREIGN KEY ("listaId") REFERENCES MIS."listas" ("id");
alter table MIS."perfiles"
    add CONSTRAINT "FK_perfiles_rolId" FOREIGN KEY ("rolId") REFERENCES MIS."roles" ("id");
alter table MIS."listas_campos_config"
    add CONSTRAINT "FK_list_camp_config_listaId" FOREIGN KEY ("listaId") REFERENCES MIS."listas" ("id") ON delete CASCADE;
alter table MIS."listas_campos_config"
    add CONSTRAINT "FK_list_camp_config_campoId" FOREIGN KEY ("campoId") REFERENCES MIS."campos" ("id");
alter table MIS."conexiones_externas"
    add CONSTRAINT "FK_conex_ext_listaId" FOREIGN KEY ("listaId") REFERENCES MIS."listas" ("id");
alter table MIS."usuarios_roles"
    add CONSTRAINT "FK_usu_roles_usuariosId" FOREIGN KEY ("usuariosId") REFERENCES MIS."usuarios" ("id") ON delete CASCADE;
alter table MIS."usuarios_roles"
    add CONSTRAINT "FK_usu_roles_rolesId" FOREIGN KEY ("rolesId") REFERENCES MIS."roles" ("id") ON delete CASCADE;
alter table MIS."usuarios_areas"
    add CONSTRAINT "FK_usu_areas_usuariosId" FOREIGN KEY ("usuariosId") REFERENCES MIS."usuarios" ("id") ON delete CASCADE;
alter table MIS."usuarios_areas"
    add CONSTRAINT "FK_usu_areas_areasId" FOREIGN KEY ("areasId") REFERENCES MIS."areas" ("id") ON delete CASCADE;
alter table MIS."campos_perfiles"
    add CONSTRAINT "FK_camp_perf_camposId" FOREIGN KEY ("camposId") REFERENCES MIS."campos" ("id") ON delete CASCADE;
alter table MIS."campos_perfiles"
    add CONSTRAINT "FK_camp_perf_perfilesId" FOREIGN KEY ("perfilesId") REFERENCES MIS."perfiles" ("id") ON delete CASCADE;
alter table MIS."listas_responsables"
    add CONSTRAINT "FK_list_resp_listasId" FOREIGN KEY ("listasId") REFERENCES MIS."listas" ("id") ON delete CASCADE;
alter table MIS."listas_responsables"
    add CONSTRAINT "FK_list_resp_usuariosId" FOREIGN KEY ("usuariosId") REFERENCES MIS."usuarios" ("id") ON delete CASCADE;
commit;


SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
grant INDEX on "MIS"."usuarios" to "USRPREVLIST";
grant INDEX on "MIS"."perfiles" to "USRPREVLIST";
grant INDEX on "MIS"."campos_subcategorias" to "USRPREVLIST";
grant INDEX on "MIS"."listas" to "USRPREVLIST";
grant INDEX on "MIS"."usuarios_areas" to "USRPREVLIST";
grant INDEX on "MIS"."listas_campos_config" to "USRPREVLIST";
grant INDEX on "MIS"."cargas_masivas" to "USRPREVLIST";
grant INDEX on "MIS"."campos" to "USRPREVLIST";
grant INDEX on "MIS"."listas_responsables" to "USRPREVLIST";
grant INDEX on "MIS"."areas" to "USRPREVLIST";
grant INDEX on "MIS"."transacciones" to "USRPREVLIST";
grant INDEX on "MIS"."usuarios_roles" to "USRPREVLIST";
grant INDEX on "MIS"."transacciones_detalle" to "USRPREVLIST";
grant INDEX on "MIS"."auditoria" to "USRPREVLIST";
grant INDEX on "MIS"."conexiones_externas" to "USRPREVLIST";
grant INDEX on "MIS"."roles" to "USRPREVLIST";
grant INDEX on "MIS"."campos_perfiles" to "USRPREVLIST";
grant DELETE on "MIS"."usuarios" to "USRPREVLIST";
grant DELETE on "MIS"."perfiles" to "USRPREVLIST";
grant DELETE on "MIS"."campos_subcategorias" to "USRPREVLIST";
grant DELETE on "MIS"."listas" to "USRPREVLIST";
grant DELETE on "MIS"."usuarios_areas" to "USRPREVLIST";
grant DELETE on "MIS"."listas_campos_config" to "USRPREVLIST";
grant DELETE on "MIS"."cargas_masivas" to "USRPREVLIST";
grant DELETE on "MIS"."campos" to "USRPREVLIST";
grant DELETE on "MIS"."listas_responsables" to "USRPREVLIST";
grant DELETE on "MIS"."areas" to "USRPREVLIST";
grant DELETE on "MIS"."transacciones" to "USRPREVLIST";
grant DELETE on "MIS"."usuarios_roles" to "USRPREVLIST";
grant DELETE on "MIS"."transacciones_detalle" to "USRPREVLIST";
grant DELETE on "MIS"."auditoria" to "USRPREVLIST";
grant DELETE on "MIS"."conexiones_externas" to "USRPREVLIST";
grant DELETE on "MIS"."roles" to "USRPREVLIST";
grant DELETE on "MIS"."campos_perfiles" to "USRPREVLIST";
grant ALTER on "MIS"."usuarios" to "USRPREVLIST";
grant ALTER on "MIS"."perfiles" to "USRPREVLIST";
grant ALTER on "MIS"."campos_subcategorias" to "USRPREVLIST";
grant ALTER on "MIS"."listas" to "USRPREVLIST";
grant ALTER on "MIS"."usuarios_areas" to "USRPREVLIST";
grant ALTER on "MIS"."listas_campos_config" to "USRPREVLIST";
grant ALTER on "MIS"."cargas_masivas" to "USRPREVLIST";
grant ALTER on "MIS"."campos" to "USRPREVLIST";
grant ALTER on "MIS"."listas_responsables" to "USRPREVLIST";
grant ALTER on "MIS"."areas" to "USRPREVLIST";
grant ALTER on "MIS"."transacciones" to "USRPREVLIST";
grant ALTER on "MIS"."usuarios_roles" to "USRPREVLIST";
grant ALTER on "MIS"."transacciones_detalle" to "USRPREVLIST";
grant ALTER on "MIS"."auditoria" to "USRPREVLIST";
grant ALTER on "MIS"."conexiones_externas" to "USRPREVLIST";
grant ALTER on "MIS"."roles" to "USRPREVLIST";
grant ALTER on "MIS"."campos_perfiles" to "USRPREVLIST";
grant DEBUG on "MIS"."usuarios" to "USRPREVLIST";
grant DEBUG on "MIS"."perfiles" to "USRPREVLIST";
grant DEBUG on "MIS"."campos_subcategorias" to "USRPREVLIST";
grant DEBUG on "MIS"."listas" to "USRPREVLIST";
grant DEBUG on "MIS"."usuarios_areas" to "USRPREVLIST";
grant DEBUG on "MIS"."listas_campos_config" to "USRPREVLIST";
grant DEBUG on "MIS"."cargas_masivas" to "USRPREVLIST";
grant DEBUG on "MIS"."campos" to "USRPREVLIST";
grant DEBUG on "MIS"."listas_responsables" to "USRPREVLIST";
grant DEBUG on "MIS"."areas" to "USRPREVLIST";
grant DEBUG on "MIS"."transacciones" to "USRPREVLIST";
grant DEBUG on "MIS"."usuarios_roles" to "USRPREVLIST";
grant DEBUG on "MIS"."transacciones_detalle" to "USRPREVLIST";
grant DEBUG on "MIS"."auditoria" to "USRPREVLIST";
grant DEBUG on "MIS"."conexiones_externas" to "USRPREVLIST";
grant DEBUG on "MIS"."roles" to "USRPREVLIST";
grant DEBUG on "MIS"."campos_perfiles" to "USRPREVLIST";
grant INSERT on "MIS"."usuarios" to "USRPREVLIST";
grant INSERT on "MIS"."perfiles" to "USRPREVLIST";
grant INSERT on "MIS"."campos_subcategorias" to "USRPREVLIST";
grant INSERT on "MIS"."listas" to "USRPREVLIST";
grant INSERT on "MIS"."usuarios_areas" to "USRPREVLIST";
grant INSERT on "MIS"."listas_campos_config" to "USRPREVLIST";
grant INSERT on "MIS"."cargas_masivas" to "USRPREVLIST";
grant INSERT on "MIS"."campos" to "USRPREVLIST";
grant INSERT on "MIS"."listas_responsables" to "USRPREVLIST";
grant INSERT on "MIS"."areas" to "USRPREVLIST";
grant INSERT on "MIS"."transacciones" to "USRPREVLIST";
grant INSERT on "MIS"."usuarios_roles" to "USRPREVLIST";
grant INSERT on "MIS"."transacciones_detalle" to "USRPREVLIST";
grant INSERT on "MIS"."auditoria" to "USRPREVLIST";
grant INSERT on "MIS"."conexiones_externas" to "USRPREVLIST";
grant INSERT on "MIS"."roles" to "USRPREVLIST";
grant INSERT on "MIS"."campos_perfiles" to "USRPREVLIST";
grant SELECT on "MIS"."usuarios" to "USRPREVLIST";
grant SELECT on "MIS"."perfiles" to "USRPREVLIST";
grant SELECT on "MIS"."campos_subcategorias" to "USRPREVLIST";
grant SELECT on "MIS"."listas" to "USRPREVLIST";
grant SELECT on "MIS"."usuarios_areas" to "USRPREVLIST";
grant SELECT on "MIS"."listas_campos_config" to "USRPREVLIST";
grant SELECT on "MIS"."cargas_masivas" to "USRPREVLIST";
grant SELECT on "MIS"."campos" to "USRPREVLIST";
grant SELECT on "MIS"."listas_responsables" to "USRPREVLIST";
grant SELECT on "MIS"."areas" to "USRPREVLIST";
grant SELECT on "MIS"."transacciones" to "USRPREVLIST";
grant SELECT on "MIS"."usuarios_roles" to "USRPREVLIST";
grant SELECT on "MIS"."transacciones_detalle" to "USRPREVLIST";
grant SELECT on "MIS"."auditoria" to "USRPREVLIST";
grant SELECT on "MIS"."conexiones_externas" to "USRPREVLIST";
grant SELECT on "MIS"."roles" to "USRPREVLIST";
grant SELECT on "MIS"."campos_perfiles" to "USRPREVLIST";

grant READ on "MIS"."usuarios" to "USRPREVLIST";
grant READ on "MIS"."perfiles" to "USRPREVLIST";
grant READ on "MIS"."campos_subcategorias" to "USRPREVLIST";
grant READ on "MIS"."listas" to "USRPREVLIST";
grant READ on "MIS"."usuarios_areas" to "USRPREVLIST";
grant READ on "MIS"."listas_campos_config" to "USRPREVLIST";
grant READ on "MIS"."cargas_masivas" to "USRPREVLIST";
grant READ on "MIS"."campos" to "USRPREVLIST";
grant READ on "MIS"."listas_responsables" to "USRPREVLIST";
grant READ on "MIS"."areas" to "USRPREVLIST";
grant READ on "MIS"."transacciones" to "USRPREVLIST";
grant READ on "MIS"."usuarios_roles" to "USRPREVLIST";
grant READ on "MIS"."transacciones_detalle" to "USRPREVLIST";
grant READ on "MIS"."auditoria" to "USRPREVLIST";
grant READ on "MIS"."conexiones_externas" to "USRPREVLIST";
grant READ on "MIS"."roles" to "USRPREVLIST";
grant READ on "MIS"."campos_perfiles" to "USRPREVLIST";
grant UPDATE on "MIS"."usuarios" to "USRPREVLIST";
grant UPDATE on "MIS"."perfiles" to "USRPREVLIST";
grant UPDATE on "MIS"."campos_subcategorias" to "USRPREVLIST";
grant UPDATE on "MIS"."listas" to "USRPREVLIST";
grant UPDATE on "MIS"."usuarios_areas" to "USRPREVLIST";
grant UPDATE on "MIS"."listas_campos_config" to "USRPREVLIST";
grant UPDATE on "MIS"."cargas_masivas" to "USRPREVLIST";
grant UPDATE on "MIS"."campos" to "USRPREVLIST";
grant UPDATE on "MIS"."listas_responsables" to "USRPREVLIST";
grant UPDATE on "MIS"."areas" to "USRPREVLIST";
grant UPDATE on "MIS"."transacciones" to "USRPREVLIST";
grant UPDATE on "MIS"."usuarios_roles" to "USRPREVLIST";
grant UPDATE on "MIS"."transacciones_detalle" to "USRPREVLIST";
grant UPDATE on "MIS"."auditoria" to "USRPREVLIST";
grant UPDATE on "MIS"."conexiones_externas" to "USRPREVLIST";
grant UPDATE on "MIS"."roles" to "USRPREVLIST";
grant UPDATE on "MIS"."campos_perfiles" to "USRPREVLIST";
grant REFERENCES on "MIS"."usuarios" to "USRPREVLIST";
grant REFERENCES on "MIS"."perfiles" to "USRPREVLIST";
grant REFERENCES on "MIS"."campos_subcategorias" to "USRPREVLIST";
grant REFERENCES on "MIS"."listas" to "USRPREVLIST";
grant REFERENCES on "MIS"."usuarios_areas" to "USRPREVLIST";
grant REFERENCES on "MIS"."listas_campos_config" to "USRPREVLIST";
grant REFERENCES on "MIS"."cargas_masivas" to "USRPREVLIST";
grant REFERENCES on "MIS"."campos" to "USRPREVLIST";
grant REFERENCES on "MIS"."listas_responsables" to "USRPREVLIST";
grant REFERENCES on "MIS"."areas" to "USRPREVLIST";
grant REFERENCES on "MIS"."transacciones" to "USRPREVLIST";
grant REFERENCES on "MIS"."usuarios_roles" to "USRPREVLIST";
grant REFERENCES on "MIS"."transacciones_detalle" to "USRPREVLIST";
grant REFERENCES on "MIS"."auditoria" to "USRPREVLIST";
grant REFERENCES on "MIS"."conexiones_externas" to "USRPREVLIST";
grant REFERENCES on "MIS"."roles" to "USRPREVLIST";
grant REFERENCES on "MIS"."campos_perfiles" to "USRPREVLIST";
grant FLASHBACK on "MIS"."usuarios" to "USRPREVLIST";
grant FLASHBACK on "MIS"."perfiles" to "USRPREVLIST";
grant FLASHBACK on "MIS"."campos_subcategorias" to "USRPREVLIST";
grant FLASHBACK on "MIS"."listas" to "USRPREVLIST";
grant FLASHBACK on "MIS"."usuarios_areas" to "USRPREVLIST";
grant FLASHBACK on "MIS"."listas_campos_config" to "USRPREVLIST";
grant FLASHBACK on "MIS"."cargas_masivas" to "USRPREVLIST";
grant FLASHBACK on "MIS"."campos" to "USRPREVLIST";
grant FLASHBACK on "MIS"."listas_responsables" to "USRPREVLIST";
grant FLASHBACK on "MIS"."areas" to "USRPREVLIST";
grant FLASHBACK on "MIS"."transacciones" to "USRPREVLIST";
grant FLASHBACK on "MIS"."usuarios_roles" to "USRPREVLIST";
grant FLASHBACK on "MIS"."transacciones_detalle" to "USRPREVLIST";
grant FLASHBACK on "MIS"."auditoria" to "USRPREVLIST";
grant FLASHBACK on "MIS"."conexiones_externas" to "USRPREVLIST";
grant FLASHBACK on "MIS"."roles" to "USRPREVLIST";
grant FLASHBACK on "MIS"."campos_perfiles" to "USRPREVLIST";
grant ON COMMIT REFRESH on "MIS"."usuarios" to "USRPREVLIST";
grant ON COMMIT REFRESH on "MIS"."perfiles" to "USRPREVLIST";
grant ON COMMIT REFRESH on "MIS"."campos_subcategorias" to "USRPREVLIST";
grant ON COMMIT REFRESH on "MIS"."listas" to "USRPREVLIST";
grant ON COMMIT REFRESH on "MIS"."usuarios_areas" to "USRPREVLIST";
grant ON COMMIT REFRESH on "MIS"."listas_campos_config" to "USRPREVLIST";
grant ON COMMIT REFRESH on "MIS"."cargas_masivas" to "USRPREVLIST";
grant ON COMMIT REFRESH on "MIS"."campos" to "USRPREVLIST";
grant ON COMMIT REFRESH on "MIS"."listas_responsables" to "USRPREVLIST";
grant ON COMMIT REFRESH on "MIS"."areas" to "USRPREVLIST";
grant ON COMMIT REFRESH on "MIS"."transacciones" to "USRPREVLIST";
grant ON COMMIT REFRESH on "MIS"."usuarios_roles" to "USRPREVLIST";
grant ON COMMIT REFRESH on "MIS"."transacciones_detalle" to "USRPREVLIST";
grant ON COMMIT REFRESH on "MIS"."auditoria" to "USRPREVLIST";
grant ON COMMIT REFRESH on "MIS"."conexiones_externas" to "USRPREVLIST";
grant ON COMMIT REFRESH on "MIS"."roles" to "USRPREVLIST";
grant ON COMMIT REFRESH on "MIS"."campos_perfiles" to "USRPREVLIST";
grant QUERY REWRITE on "MIS"."usuarios" to "USRPREVLIST";
grant QUERY REWRITE on "MIS"."perfiles" to "USRPREVLIST";
grant QUERY REWRITE on "MIS"."campos_subcategorias" to "USRPREVLIST";
grant QUERY REWRITE on "MIS"."listas" to "USRPREVLIST";
grant QUERY REWRITE on "MIS"."usuarios_areas" to "USRPREVLIST";
grant QUERY REWRITE on "MIS"."listas_campos_config" to "USRPREVLIST";
grant QUERY REWRITE on "MIS"."cargas_masivas" to "USRPREVLIST";
grant QUERY REWRITE on "MIS"."campos" to "USRPREVLIST";
grant QUERY REWRITE on "MIS"."listas_responsables" to "USRPREVLIST";
grant QUERY REWRITE on "MIS"."areas" to "USRPREVLIST";
grant QUERY REWRITE on "MIS"."transacciones" to "USRPREVLIST";
grant QUERY REWRITE on "MIS"."usuarios_roles" to "USRPREVLIST";
grant QUERY REWRITE on "MIS"."transacciones_detalle" to "USRPREVLIST";
grant QUERY REWRITE on "MIS"."auditoria" to "USRPREVLIST";
grant QUERY REWRITE on "MIS"."conexiones_externas" to "USRPREVLIST";
grant QUERY REWRITE on "MIS"."roles" to "USRPREVLIST";
grant QUERY REWRITE on "MIS"."campos_perfiles" to "USRPREVLIST";

commit;

SET TRANSACTION ISOLATION LEVEL READ COMMITTED;
create  synonym campos_subcategorias for MIS."campos_subcategorias";
create  synonym transacciones_detalle for MIS."transacciones_detalle";
create  synonym transacciones for MIS."transacciones";
create  synonym cargas_masivas for MIS."cargas_masivas";
create  synonym listas_campos_config for MIS."listas_campos_config";
create  synonym conexiones_externas for MIS."conexiones_externas";
create  synonym usuarios_roles for MIS."usuarios_roles";
create  synonym usuarios_areas for MIS."usuarios_areas";
create  synonym areas for MIS."areas";
create  synonym campos_perfiles for MIS."campos_perfiles";
create  synonym perfiles for MIS."perfiles";
create  synonym roles for MIS."roles";
create  synonym campos for MIS."campos";
create  synonym listas_responsables for MIS."listas_responsables";
create  synonym usuarios for MIS."usuarios";
create  synonym listas for MIS."listas";
create  synonym auditoria for MIS."auditoria";
commit ;
