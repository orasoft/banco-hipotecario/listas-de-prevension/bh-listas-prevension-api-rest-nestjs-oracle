import { EntitySubscriberInterface, EventSubscriber, InsertEvent } from 'typeorm';
import { TransaccionDetalle } from '../entities';
import { Logger } from '@nestjs/common';

@EventSubscriber()
export class TransaccionDetalleSubscriber implements EntitySubscriberInterface<TransaccionDetalle> {

  /**
   * Indicates that this subscriber only listen to Post events.
   */
  listenTo() {
    return TransaccionDetalle;
  }

  /**
   * Called before post insertion.
   */
  afterInsert(event: InsertEvent<TransaccionDetalle>) {
    Logger.log(`Transaccion Detalle => Insert: ${JSON.stringify(event.entity)}`);
  }
}
