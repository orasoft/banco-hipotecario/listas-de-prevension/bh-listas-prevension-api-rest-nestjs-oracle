import { EntitySubscriberInterface, EventSubscriber, InsertEvent } from 'typeorm';
import { Campo } from '../entities';
import { Logger } from '@nestjs/common';

@EventSubscriber()
export class CampoSubscriber implements EntitySubscriberInterface<Campo> {

  /**
   * Indicates that this subscriber only listen to Post events.
   */
  listenTo() {
    return Campo;
  }

  /**
   * Called before post insertion.
   */
  beforeInsert(event: InsertEvent<Campo>) {
    Logger.log(`Campos => Insert: ${event.entity}`);
  }
}
