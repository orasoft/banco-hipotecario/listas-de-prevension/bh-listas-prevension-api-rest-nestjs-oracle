import { EntitySubscriberInterface, EventSubscriber, InsertEvent } from 'typeorm';
import { Transaccion } from '../entities';
import { Logger } from '@nestjs/common';

@EventSubscriber()
export class TransaccionSubscriber implements EntitySubscriberInterface<Transaccion> {
  listenTo() {
    return Transaccion;
  }

  afterInsert(event: InsertEvent<Transaccion>) {
    Logger.log(`Transaccion => Insert: ${JSON.stringify(event.entity)}`);
  }
}
