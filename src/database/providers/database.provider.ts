import { createConnection } from 'typeorm';
import {
  Area,
  Campo,
  CampoSubcategoria,
  CargaMasiva,
  ConexionExterna,
  Lista,
  ListaCampoConfig,
  Perfil,
  Rol, Transaccion,
  TransaccionDetalle,
  Usuario,
} from '../entities';
import { ConfigService } from '../../config/config.service';

export const DatabaseProvider = [
  {
    provide: 'DATABASE_CONNECTION',
    useFactory: async () => {
      const config = new ConfigService('.env');
      const configDatabase = config.getConfigDatabase();
      return createConnection({
        type: 'oracle',
        host: configDatabase.HOST,
        port: Number(configDatabase.PORT),
        username: configDatabase.USER,
        password: configDatabase.PASSWORD,
        sid: configDatabase.SID,
        schema: configDatabase.SCHEMA,
        entities: [
          Lista,
          Campo,
          CampoSubcategoria,
          ListaCampoConfig,
          Usuario,
          Rol,
          Perfil,
          ConexionExterna,
          Transaccion,
          TransaccionDetalle,
          CargaMasiva,
          Area],
        synchronize: false,
        logging: true,
        subscribers: [],
      });
    },
  },
];
