import { Module } from '@nestjs/common';
import { DatabaseProvider } from './providers/database.provider';
import { ConfigModule } from '../config/config.module';

@Module({
  imports: [ConfigModule],
  providers: [...DatabaseProvider],
  exports: [...DatabaseProvider],
})
export class DatabaseModule {
}
