import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  Index,
  ManyToMany,
  ManyToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { Lista } from './lista';
import { Rol } from './rol';
import { Campo } from './campo';

export enum PerfilAcceso {
  consulta = 'Consulta',
  edicion = 'Edicion',
  ninguno = 'Ninguno',
}

export enum PerfilVisualizar {
  vigente = 'Vigente',
  historico = 'Historico',
}

@Entity('perfiles')
@Index(['lista', 'rol'], { unique: true })
export class Perfil {

  @PrimaryColumn()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { length: 10 })
  acceso: PerfilAcceso;

  @Column('varchar', { length: 10 })
  visualizar: PerfilVisualizar;

  @Column()
  descargar: boolean;

  @Column()
  responsable: boolean;

  @VersionColumn() version: number;

  @CreateDateColumn({ nullable: false })
  createdAt: Date;

  @Column('varchar', { nullable: false, length: 10 })
  createdBy: string;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @Column('varchar', { nullable: true, length: 10 })
  updatedBy: string;

  @ManyToOne(type => Lista, lista => lista.perfiles)
  lista: Lista;

  @ManyToOne(type => Rol, rol => rol.perfiles)
  rol: Rol;

  @ManyToMany(type => Campo, campo => campo.perfiles)
  campos: Campo[];

  @BeforeInsert()
  insertDates() {
    this.createdAt = new Date();
  }

  @BeforeUpdate()
  updateDates() {
    this.updatedAt = new Date();
  }
}
