import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  Index,
  ManyToOne,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { Lista } from './lista';
import { Campo } from './campo';

export enum ListaCampoConfigEstado {
  activa = 'A',
  inactiva = 'I',
}

@Entity('listas_campos_config')
@Index(['lista', 'campo'], { unique: true })
export class ListaCampoConfig {

  @PrimaryColumn()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  requerido: boolean;

  @Column()
  busqueda: boolean;

  @Column()
  orden: number;

  @Column('varchar', { length: 1 })
  estado: ListaCampoConfigEstado;

  @ManyToOne(() => Lista, (lista: Lista) => lista.camposConfigurados, { onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  lista: Lista;

  @ManyToOne(() => Campo, (campo: Campo) => campo.camposConfigurados)
  campo: Campo;

  @VersionColumn() version: number;

  @CreateDateColumn({ nullable: false })
  createdAt: Date;

  @Column('varchar', { nullable: false, length: 10 })
  createdBy: string;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @Column('varchar', { nullable: true, length: 10 })
  updatedBy: string;

  @BeforeInsert()
  insertDates() {
    this.createdAt = new Date();
  }

  @BeforeUpdate()
  updateDates() {
    this.updatedAt = new Date();
  }
}
