import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { CampoSubcategoria } from './campoSubcategoria';
import { ListaCampoConfig } from './listaCampoConfig';
import { Perfil } from './perfil';
import { TransaccionDetalle } from './transaccionDetalle';

export enum TipoCampoValor {
  input = 'input',
  selectbox = 'selectbox',
}

export enum FormatoCampoValor {
  text = 'text',
  num = 'num',
  date = 'date',
  scat = 'scat',
}

@Entity({ name: 'campos' })
export class Campo {

  @PrimaryColumn()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { length: 50 })
  nombre: string;

  @Column('varchar', { length: 100 })
  label: string;

  @Column()
  default: boolean;

  @Column('varchar', { length: 10 })
  tipo: TipoCampoValor;

  @Column('varchar', { length: 10 })
  formato: FormatoCampoValor;

  @Column('varchar', { length: 254 })
  descripcion: string;

  @CreateDateColumn({ nullable: false })
  createdAt: Date;

  @Column('varchar', { nullable: false, length: 10 })
  createdBy: string;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @Column('varchar', { nullable: true, length: 10 })
  updatedBy: string;

  @VersionColumn() version: number;
  /*** Relaciones ***/

  @OneToMany(() => CampoSubcategoria, (subcategoria: CampoSubcategoria) => subcategoria.campo, {
    cascade: true,
    onUpdate: 'CASCADE',
    onDelete: 'CASCADE',
  })
  subcategorias: CampoSubcategoria[];

  @OneToMany(() => ListaCampoConfig, (camposConfigurados: ListaCampoConfig) => camposConfigurados.campo)
  camposConfigurados: ListaCampoConfig[];

  @ManyToMany(type => Perfil, perfil => perfil.campos)
  @JoinTable({ name: 'campos_perfiles' })
  perfiles: Perfil[];

  @OneToMany(type => TransaccionDetalle, (transaccion: TransaccionDetalle) => transaccion.campo)
  transaccionesDetalle: TransaccionDetalle[];

  /* Eventos */
  @BeforeInsert()
  insertDates() {
    this.createdAt = new Date();
  }

  @BeforeUpdate()
  updateDates() {
    this.updatedAt = new Date();
  }
}
