import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { ListaCampoConfig } from './listaCampoConfig';
import { Usuario } from './usuario';
import { Perfil } from './perfil';
import { CargaMasiva } from './cargaMasiva';

export enum TipoLista {
  interna = 'Interna',
  externa = 'Externa',
}

export enum EstadoLista {
  activa = 'A',
  inactiva = 'I',
}

@Entity({ name: 'listas' })
export class Lista {

  @PrimaryColumn()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ length: 20, unique: true })
  nombre: string;

  @Column({ length: 254, nullable: true })
  descripcion: string;

  @Column('varchar', { length: 10 })
  tipo: TipoLista;

  @Column('varchar', { length: 1 })
  estado: EstadoLista;

  @Column()
  criticidad: string;

  @Column()
  observaciones: string;

  @ManyToMany(type => Usuario, usuario => usuario.listas, { cascade: true })
  @JoinTable({ name: 'listas_responsables' })
  responsables: Usuario[];

  @CreateDateColumn({ nullable: false })
  createdAt: Date;

  @Column('varchar', { nullable: false, length: 10 })
  createdBy: string;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @Column('varchar', { nullable: true, length: 10 })
  updatedBy: string;

  @VersionColumn() version: number;

  @OneToMany(() => ListaCampoConfig, (campoConfigurado: ListaCampoConfig) => campoConfigurado.lista, {
    cascade: true,
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE',
  })
  camposConfigurados: ListaCampoConfig[];

  @OneToMany(type => Perfil, perfil => perfil.lista, { cascade: true, onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  perfiles: Perfil[];

  @OneToMany(() => CargaMasiva, (carga: CargaMasiva) => carga.lista)
  cargas: CargaMasiva[];

  @BeforeInsert()
  insertDates() {
    this.createdAt = new Date();
  }

  @BeforeUpdate()
  updateDates() {
    this.updatedAt = new Date();
  }
}
