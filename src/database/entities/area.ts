import { CargaMasiva } from './cargaMasiva';
import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  ManyToMany,
  OneToMany,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { Usuario } from './usuario';

export enum AreaEstado {
  activa = 'A',
  desactivada = 'D',
}

@Entity('areas')
export class Area {
  @PrimaryColumn()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { length: 50 })
  nombre: string;

  @Column('varchar', { length: 1 })
  estado: AreaEstado;

  @CreateDateColumn({ nullable: false })
  createdAt: Date;

  @Column('varchar', { nullable: false, length: 10 })
  createdBy: string;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @Column('varchar', { nullable: true, length: 10 })
  updatedBy: string;

  @VersionColumn() version: number;

  @ManyToMany(type => Usuario, responsable => responsable.areas)
  responsables: Usuario[];

  @OneToMany(type => CargaMasiva, (carga: CargaMasiva) => carga.area)
  cargas: CargaMasiva[];

  /* Eventos */
  @BeforeInsert()
  insertDates() {
    this.createdAt = new Date();
  }

  @BeforeUpdate()
  updateDates() {
    this.updatedAt = new Date();
  }
}
