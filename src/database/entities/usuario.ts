import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  JoinTable,
  ManyToMany,
  OneToMany,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { Lista } from './lista';
import { Rol } from './rol';
import { Area } from './area';
import { CargaMasiva } from './cargaMasiva';

@Entity('usuarios')
export class Usuario {

  @PrimaryColumn()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true })
  codbh: string;

  @Column()
  usuario: string;

  @ManyToMany(type => Lista, lista => lista.responsables)
  listas: Lista[];

  @ManyToMany(type => Rol, rol => rol.usuarios)
  @JoinTable({ name: 'usuarios_roles' })
  roles: Rol[];

  @ManyToMany(type => Area, area => area.responsables)
  @JoinTable({ name: 'usuarios_areas' })
  areas: Area[];

  @OneToMany(type => CargaMasiva, (carga: CargaMasiva) => carga.usuario)
  cargas: CargaMasiva[];

  @CreateDateColumn({ nullable: false })
  createdAt: Date;

  @Column('varchar', { nullable: false, length: 10 })
  createdBy: string;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @Column('varchar', { nullable: true, length: 10 })
  updatedBy: string;

  @VersionColumn() version: number;

  /* Eventos */
  @BeforeInsert()
  insertDates() {
    this.createdAt = new Date();
  }

  @BeforeUpdate()
  updateDates() {
    this.updatedAt = new Date();
  }
}
