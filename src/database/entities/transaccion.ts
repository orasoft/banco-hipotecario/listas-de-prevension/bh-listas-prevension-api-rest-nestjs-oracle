import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { CargaMasiva } from './cargaMasiva';
import { TransaccionDetalle } from './transaccionDetalle';

@Entity({ name: 'transacciones' })
export class Transaccion {

  @PrimaryColumn()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @ManyToOne(() => CargaMasiva, (cargaMasiva: CargaMasiva) => cargaMasiva.transacciones)
  cargaMasiva: CargaMasiva;

  @OneToMany(() => TransaccionDetalle, (detalle: TransaccionDetalle) => detalle.transaccion)
  detalle: TransaccionDetalle[];

  @Column('varchar', { nullable: true, length: 255 })
  justificacion: string;

  @Column('varchar', { nullable: true, length: 1})
  estado: string;

  @CreateDateColumn({ nullable: false })
  createdAt: Date;

  @Column('varchar', { nullable: false, length: 10 })
  createdBy: string;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @Column('varchar', { nullable: true, length: 10 })
  updatedBy: string;

  @VersionColumn() version: number;

  /* Eventos */
  @BeforeInsert()
  insertDates() {
    this.createdAt = new Date();
    this.estado = 'A';
  }

  @BeforeUpdate()
  updateDates() {
    this.updatedAt = new Date();
  }
}
