import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  ManyToMany,
  OneToMany,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { Usuario } from './usuario';
import { Perfil } from './perfil';

@Entity('roles')
export class Rol {

  @PrimaryColumn()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ unique: true })
  idBhRol: number;

  @Column()
  nombre: string;

  @ManyToMany(type => Usuario, usuario => usuario.roles)
  usuarios: Usuario[];

  @OneToMany(type => Perfil, perfil => perfil.rol, { cascade: true, onDelete: 'CASCADE', onUpdate: 'CASCADE' })
  perfiles: Perfil[];

  @CreateDateColumn({ nullable: false })
  createdAt: Date;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @VersionColumn() version: number;

  /* Eventos */
  @BeforeInsert()
  insertDates() {
    this.createdAt = new Date();
  }

  @BeforeUpdate()
  updateDates() {
    this.updatedAt = new Date();
  }
}
