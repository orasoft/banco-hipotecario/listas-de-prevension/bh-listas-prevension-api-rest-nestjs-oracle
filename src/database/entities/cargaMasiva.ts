import {
  BeforeInsert,
  BeforeUpdate,
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  VersionColumn,
} from 'typeorm';
import { Area } from './area';
import { Usuario } from './usuario';
import { Transaccion } from './transaccion';
import { Lista } from './lista';

export enum EstadoCargaMasiva {
  activa = 'A',
  desactivada = 'N',
}

export enum FormaCarga {
  masiva = 'M',
  individual = 'I',
}

@Entity('cargas_masivas')
export class CargaMasiva {

  @PrimaryColumn()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('varchar', { length: 1 })
  estado: EstadoCargaMasiva;

  @Column('varchar', { length: 1 })
  forma: FormaCarga;

  @Column({ nullable: true })
  motivo: string;

  @CreateDateColumn({ nullable: false })
  createdAt: Date;

  @Column('varchar', { nullable: false, length: 10 })
  createdBy: string;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @Column('varchar', { nullable: true, length: 10 })
  updatedBy: string;

  @VersionColumn() version: number;

  @ManyToOne(type => Area, (area: Area) => area.cargas)
  area: Area;

  @ManyToOne(type => Usuario, (usuario: Usuario) => usuario.cargas)
  usuario: Usuario;

  @ManyToOne(type => Lista, (lista: Lista) => lista.cargas)
  lista: Lista;

  @OneToMany(type => Transaccion, (transaccion: Transaccion) => transaccion.cargaMasiva)
  transacciones: Transaccion[];

  /* Eventos */
  @BeforeInsert()
  insertDates() {
    this.createdAt = new Date();
  }

  @BeforeUpdate()
  updateDates() {
    this.updatedAt = new Date();
  }
}
