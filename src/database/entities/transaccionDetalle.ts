import { Column, CreateDateColumn, Entity, ManyToOne, PrimaryColumn, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import { Campo } from './campo';
import { Transaccion } from './transaccion';

@Entity('transacciones_detalle')
export class TransaccionDetalle {

  @PrimaryColumn()
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  valor: string;

  @CreateDateColumn({ nullable: false })
  createdAt: Date;

  @Column('varchar', { nullable: false, length: 10 })
  createdBy: string;

  @UpdateDateColumn({ nullable: true })
  updatedAt: Date;

  @Column('varchar', { nullable: true, length: 10 })
  updatedBy: string;

  @ManyToOne(type => Campo, (campo: Campo) => campo.transaccionesDetalle)
  campo: Campo;

  @ManyToOne(type => Transaccion, (transaccion: Transaccion) => transaccion.detalle)
  transaccion: Transaccion;
}
