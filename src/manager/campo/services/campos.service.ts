import { BadRequestException, Inject, Injectable, Logger } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Campo, CampoSubcategoria, FormatoCampoValor, TipoCampoValor } from '../../../database/entities';
import { CreacionCampoRequest } from '../dto/creacion.campo.request';
import { UpdateCampoRequest } from '../dto/update.campo.request';
import { SubcategoriaService } from '../../categoria/services/subcategoria.service';
import { CampoMessage } from '../dto/campo.message';

@Injectable()
export class CamposService {
  constructor(@Inject('CAMPOS_REPOSITORY') private readonly repositorio: Repository<Campo>) {
  }

  static setMessage(campo: Campo) {
    const message = new CampoMessage();
    message.id = campo.id;
    message.descripcion = campo.descripcion;
    message.formato = campo.formato;
    message.camposConfigurados = campo.camposConfigurados;
    message.createdAt = campo.createdAt;
    message.label = campo.label;
    message.nombre = campo.nombre;
    message.perfiles = campo.perfiles;
    message.predeterminado = campo.default;
    message.tipo = campo.tipo;
    message.transaccionesDetalle = campo.transaccionesDetalle;
    message.version = campo.version;
    message.updatedAt = campo.updatedAt;

    const subcategorias = [];
    campo.subcategorias.forEach(subcategoria => {
      subcategorias.push(SubcategoriaService.setupRespuesta(subcategoria));
    });
    message.subcategorias = subcategorias;
    return message;
  }

  async findAll() {
    const [items, total] = await this.repositorio.findAndCount({ relations: ['subcategorias'] });
    const campos = [];
    items.forEach(campo => {
      campos.push(CamposService.setMessage(campo));
    });
    return {
      total,
      items: campos,
    };
  }

  async findById(id: string): Promise<Campo> {
    return await this.repositorio.findOneOrFail(id, { relations: ['subcategorias'] });
  }

  async findByIdWithCache(id: string): Promise<Campo> {
    return await this.repositorio.findOneOrFail(id, { relations: ['subcategorias'], cache: true });
  }

  async findByName(name: string) {
    return await this.repositorio.find({ where: { nombre: name }, relations: ['subcategorias'] });
  }

  async findByPredeterminado() {
    return await this.repositorio.find({ where: { default: true }, relations: ['subcategorias'] });
  }

  async create(request: CreacionCampoRequest): Promise<Campo> {

    if (request.tipo === TipoCampoValor.selectbox) {
      if (request.formato !== FormatoCampoValor.scat) {
        throw new BadRequestException();
      } else {
        if (request.subcategorias.length === 0) {
          throw new BadRequestException();
        }
      }
    }

    const registro: Campo = this.asignacion(request);
    registro.createdBy = request.createdBy;

    Logger.verbose(`Campo a Guardar => ${JSON.stringify(registro)}`);

    return this.repositorio.manager.save(registro);
  }

  async update(id: string, registro: UpdateCampoRequest): Promise<Campo> {
    if (id === registro.id) {
      const recordFound = await this.repositorio.findOneOrFail(registro.id);
      recordFound.nombre = registro.nombre;
      recordFound.label = registro.label;
      recordFound.default = registro.predeterminado;
      recordFound.tipo = registro.tipo;
      recordFound.formato = registro.formato;
      recordFound.descripcion = registro.descripcion;
      recordFound.subcategorias = registro.subcategorias;
      recordFound.updatedBy = registro.updatedBy;
      await this.repositorio.createQueryBuilder().update().set(recordFound).execute();
      return this.repositorio.findOneOrFail(registro.id);
    }
    throw new BadRequestException();
  }

  private asignacion(request: CreacionCampoRequest): Campo {
    const campo = new Campo();
    campo.default = request.predeterminado;
    campo.formato = request.formato;
    campo.tipo = request.tipo;
    campo.descripcion = request.descripcion;
    campo.nombre = request.nombre;
    campo.subcategorias = [];
    campo.label = request.label;

    if (request.formato === FormatoCampoValor.scat) {
      request.subcategorias.forEach((categoria) => {
        const subcategoria = new CampoSubcategoria();
        subcategoria.label = categoria.label;
        subcategoria.valor = categoria.valor;
        subcategoria.createdBy = request.createdBy;
        campo.subcategorias.push(subcategoria);
      });
    }
    return campo;
  }
}
