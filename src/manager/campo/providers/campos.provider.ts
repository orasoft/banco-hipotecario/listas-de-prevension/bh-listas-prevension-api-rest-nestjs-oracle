import { Connection } from 'typeorm';
import { Campo } from '../../../database/entities/campo';

export const CamposProvider = [
  {
    provide: 'CAMPOS_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Campo),
    inject: ['DATABASE_CONNECTION'],
  },
];
