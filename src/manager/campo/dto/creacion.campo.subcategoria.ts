import { ApiModelProperty } from '@nestjs/swagger';

export class CreacionCampoSubcategoria {

  @ApiModelProperty()
  label: string;

  @ApiModelProperty()
  valor: string;
}
