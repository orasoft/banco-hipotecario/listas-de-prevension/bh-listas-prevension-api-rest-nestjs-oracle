import { CreacionCampoSubcategoria } from './creacion.campo.subcategoria';
import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateCampoSubcategoria extends CreacionCampoSubcategoria {
  @ApiModelProperty()
  id: string;
}
