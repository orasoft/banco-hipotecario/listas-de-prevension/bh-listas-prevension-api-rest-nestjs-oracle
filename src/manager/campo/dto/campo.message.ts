import { FormatoCampoValor, TipoCampoValor } from '../../../database/entities';
import { SubcategoriaMessage } from '../../categoria/dto/subcategoria.message';

export class CampoMessage {

  id: string;

  nombre: string;

  label: string;

  predeterminado: boolean;

  tipo: TipoCampoValor;

  formato: FormatoCampoValor;

  descripcion: string;

  createdAt: Date;

  updatedAt: Date;

  version: number;

  subcategorias: SubcategoriaMessage[];

  camposConfigurados: any[];

  perfiles: any[];

  transaccionesDetalle: any[];
}
