import { FormatoCampoValor, TipoCampoValor } from '../../../database/entities/campo';
import { ApiModelProperty } from '@nestjs/swagger';
import { CreacionCampoSubcategoria } from './creacion.campo.subcategoria';

export class CreacionCampoRequest {
  @ApiModelProperty()
  nombre: string;

  @ApiModelProperty()
  predeterminado: boolean;

  @ApiModelProperty({ enum: ['input', 'selectbox'] })
  tipo: TipoCampoValor;

  @ApiModelProperty({ enum: ['text', 'num', 'date', 'scat'] })
  formato: FormatoCampoValor;

  @ApiModelProperty()
  descripcion: string;

  @ApiModelProperty()
  label: string;

  @ApiModelProperty({ type: CreacionCampoSubcategoria, isArray: true })
  subcategorias: CreacionCampoSubcategoria[];

  @ApiModelProperty()
  createdBy?: string;
}
