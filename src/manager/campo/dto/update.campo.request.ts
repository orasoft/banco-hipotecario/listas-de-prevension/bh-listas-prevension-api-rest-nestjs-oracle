import { FormatoCampoValor, TipoCampoValor } from '../../../database/entities/campo';
import { ApiModelProperty } from '@nestjs/swagger';
import { CampoSubcategoria } from '../../../database/entities/campoSubcategoria';

export class UpdateCampoRequest {
  @ApiModelProperty()
  id: string;

  @ApiModelProperty()
  nombre: string;

  @ApiModelProperty()
  label: string;

  @ApiModelProperty()
  predeterminado: boolean;

  @ApiModelProperty({ type: TipoCampoValor })
  tipo: TipoCampoValor;

  @ApiModelProperty({ type: FormatoCampoValor })
  formato: FormatoCampoValor;

  @ApiModelProperty()
  descripcion: string;

  @ApiModelProperty({ isArray: true, type: CampoSubcategoria })
  subcategorias: CampoSubcategoria[];

  @ApiModelProperty()
  updatedBy: string;
}
