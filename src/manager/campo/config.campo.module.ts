import { Module } from '@nestjs/common';
import { CamposProvider } from './providers/campos.provider';
import { CamposService } from './services/campos.service';
import { DatabaseModule } from '../../database/database.module';
import { CamposController } from './controllers/campos.controller';
import { AuthModule } from '../../auth/auth.module';
import { CamposwsController } from './controllers/camposws.controller';

@Module({
  providers: [...CamposProvider, CamposService, AuthModule],
  imports: [DatabaseModule],
  controllers: [CamposController, CamposwsController],
  exports: [CamposService],
})
export class ConfigCampoModule {
}
