import { Controller, Get, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CamposService } from '../services/campos.service';

@Controller('v1/webservice')
export class CamposwsController {

  constructor(
     private readonly campoService: CamposService,
  ) {
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('fieldsSearch')
  public async findByPredeterminado() {
    return await this.campoService.findByPredeterminado();
  }
}
