import { Body, Controller, Get, HttpCode, Param, Post, Put, Request, Query, UseGuards } from '@nestjs/common';
import { CamposService } from '../services/campos.service';
import { ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { CreacionCampoRequest } from '../dto/creacion.campo.request';
import { UpdateCampoRequest } from '../dto/update.campo.request';
import { UsuarioStrategy } from '../../../auth/dtos/usuario.strategy';
import { AuthGuard } from '@nestjs/passport';

@ApiUseTags('Configuracion de Campos')
@UseGuards(AuthGuard('jwt'))
@Controller('v1/config/campos')
export class CamposController {
  constructor(private readonly service: CamposService) {
  }

  @ApiResponse({ status: 200, description: 'Se retorna la informacion de un campo en especifico por medio de su nombre.' })
  @Get('/consulta')
  public async findByName(@Query('nombre') nombre: string) {
    return await this.service.findByName(nombre);
  }

  @ApiResponse({ status: 200, description: 'Se retorna todos los campos creados pero que son obligatorios que esten en todas las listas.' })
  @Get('/predeterminados')
  public async findByPredeterminado() {
    return await this.service.findByPredeterminado();
  }

  @ApiResponse({ status: 200, description: 'Se retorna todos los campos.' })
  @Get()
  public async findAll() {
    return await this.service.findAll();
  }

  @ApiResponse({ status: 200, description: 'Se retorna la informacion de un campo en especifico por medio de su ID unico.' })
  @Get(':id')
  public async findById(@Param('id') id: string) {
    return await this.service.findById(id);
  }

  @ApiResponse({ status: 201, description: 'Creacion de campo.' })
  @Post()
  @HttpCode(201)
  public async create(@Request() request, @Body() record: CreacionCampoRequest) {
    const tokenInfo: UsuarioStrategy = request.user
    record.createdBy = tokenInfo.usuario.codbh;
    return this.service.create(record);
  }

  @ApiResponse({ status: 201, description: 'Actualizacion de un campo existente.' })
  @Put(':id')
  @HttpCode(201)
  public async update(@Request() request, @Param('id') id: string, @Body() record: UpdateCampoRequest) {
    const tokenInfo: UsuarioStrategy = request.user
    record.updatedBy = tokenInfo.usuario.codbh;
    return this.service.update(id, record);
  }
}
