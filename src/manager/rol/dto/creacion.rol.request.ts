import { ApiModelProperty } from '@nestjs/swagger';

export class CreacionRolRequest {
  @ApiModelProperty()
  idBhRol: number;

  @ApiModelProperty()
  nombre: string;
}
