import { Module } from '@nestjs/common';
import { RolesController } from './controllers/roles.controller';
import { RolesProvider } from './providers/roles.provider';
import { RolesService } from './services/roles.service';
import { DatabaseModule } from '../../database/database.module';
import { AuthModule } from '../../auth/auth.module';

@Module({
  imports: [DatabaseModule, AuthModule],
  controllers: [RolesController],
  providers: [...RolesProvider, RolesService],
  exports: [RolesService],
})
export class RolesModule {
}
