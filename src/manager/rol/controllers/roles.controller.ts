import { Body, Controller, Get, HttpCode, Post } from '@nestjs/common';
import { ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { RolesService } from '../services/roles.service';
import { CreacionRolRequest } from '../dto/creacion.rol.request';

@ApiUseTags('Mantenimiento de Roles')
@Controller('v1/config/roles')
export class RolesController {

  constructor(private readonly service: RolesService) {
  }

  @ApiResponse({ status: 201, description: 'Creacion de campo.' })
  @Post()
  @HttpCode(201)
  public async create(@Body() record: CreacionRolRequest) {
    return this.service.create(record);
  }

  @ApiResponse({ status: 200, description: 'Listado de Roles.' })
  @Get()
  @HttpCode(200)
  public async get() {
    return this.service.find();
  }

  @Post('sincronizacion')
  public async sincronizacion(@Body() record: CreacionRolRequest) {
    this.service.findByBhRol(record.idBhRol).then(exito => {
      return exito;
    }).catch(err => {
      return this.service.create(record);
    });
  }
}
