import { Connection } from 'typeorm';
import { Rol } from '../../../database/entities/rol';

export const RolesProvider = [
  {
    provide: 'ROLES_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Rol),
    inject: ['DATABASE_CONNECTION'],
  },
];
