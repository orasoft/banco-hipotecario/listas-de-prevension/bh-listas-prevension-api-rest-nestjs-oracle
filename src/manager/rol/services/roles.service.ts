import { Inject, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Rol } from '../../../database/entities';
import { CreacionRolRequest } from '../dto/creacion.rol.request';

@Injectable()
export class RolesService {
  constructor(@Inject('ROLES_REPOSITORY') private readonly repositorio: Repository<Rol>) {
  }

  async find() {
    return await this.repositorio.find({ select: ['id', 'nombre', 'idBhRol'] });
  }

  async findById(id: string) {
    return await this.repositorio.findOneOrFail(id);
  }

  async findByBhRol(idBhRol: number) {
    return await this.repositorio.findOneOrFail({ where: { idBhRol }});
  }

  async create(registro: CreacionRolRequest): Promise<Rol> {
    return this.repositorio.save(registro).then(record => {
      return record;
    });
  }

  async delete(id: string) {
    return await this.repositorio.delete(id).then(record => {
      return record;
    });
  }
}
