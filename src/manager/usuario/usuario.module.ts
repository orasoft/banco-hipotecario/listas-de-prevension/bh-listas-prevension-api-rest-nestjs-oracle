import { Module } from '@nestjs/common';
import { UsuariosController } from './controllers/usuarios.controller';
import { UsuariosProvider } from './providers/usuarios.provider';
import { UsuariosService } from './services/usuarios.service';
import { DatabaseModule } from '../../database/database.module';

@Module({
  imports: [DatabaseModule],
  controllers: [UsuariosController],
  providers: [...UsuariosProvider, UsuariosService],
  exports: [UsuariosService],
})
export class UsuarioModule {
}
