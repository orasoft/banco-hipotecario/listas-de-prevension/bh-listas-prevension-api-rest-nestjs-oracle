export class UsuarioMessage {
  id?: string;
  codbh: string;
  usuario: string;
  listas: any[];
  roles: any[];
  areas: any[];
  cargas: any[];
  creado: Date;
  actualizado: Date;
  version: number;
}
