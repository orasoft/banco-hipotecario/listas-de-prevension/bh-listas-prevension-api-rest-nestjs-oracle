import { ApiModelProperty } from '@nestjs/swagger';

export class CreacionUsuarioRequest {
  @ApiModelProperty()
  codbh: string;

  @ApiModelProperty()
  usuario: string;
}
