import { Connection } from 'typeorm';
import { Usuario } from '../../../database/entities/usuario';

export const UsuariosProvider = [
  {
    provide: 'USUARIOS_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Usuario),
    inject: ['DATABASE_CONNECTION'],
  },
];
