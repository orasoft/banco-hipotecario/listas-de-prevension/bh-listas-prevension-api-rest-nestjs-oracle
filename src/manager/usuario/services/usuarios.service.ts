import { Inject, Injectable, Logger } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Usuario } from '../../../database/entities';
import { CreacionUsuarioRequest } from '../dto/creacion.usuario.request';
import { UsuarioMessage } from '../dto/usuario.message';

@Injectable()
export class UsuariosService {
  constructor(@Inject('USUARIOS_REPOSITORY') private readonly repositorio: Repository<Usuario>) {
  }

  static setRespuesta(usuario: Usuario): Partial<UsuarioMessage> {
    const respuesta = new UsuarioMessage();
    respuesta.id = usuario.id;
    respuesta.areas = usuario.areas;
    respuesta.cargas = usuario.cargas;
    respuesta.codbh = usuario.codbh;
    respuesta.actualizado = usuario.updatedAt;
    respuesta.creado = usuario.createdAt;
    respuesta.listas = usuario.listas;
    respuesta.roles = usuario.roles;
    respuesta.version = usuario.version;
    respuesta.usuario = usuario.usuario;
    return respuesta;
  }

  async findById(id: string) {
    return await this.repositorio.findOneOrFail(id);
  }

  async findByIdWithCache(id: string) {
    return await this.repositorio.findOneOrFail(id, { cache: true });
  }

  async findByBhCode(bhcod: string) {
    return await this.repositorio.findOneOrFail({ where: { codbh: bhcod } });
  }

  async find() {
    return await this.repositorio.find({ select: ['id', 'usuario', 'codbh'] });
  }

  async create(registro: CreacionUsuarioRequest, createdBy: string): Promise<Usuario> {
    const respuesta = await this.repositorio.createQueryBuilder().insert().into(Usuario).values([
      { codbh: registro.codbh, usuario: registro.usuario, createdBy },
    ]).execute();
    const userId = respuesta.identifiers[0].id;
    return this.repositorio.findOneOrFail(userId);
  }

  async delete(id: string) {
    return await this.repositorio.delete(id).then(record => {
      return record;
    });
  }
}
