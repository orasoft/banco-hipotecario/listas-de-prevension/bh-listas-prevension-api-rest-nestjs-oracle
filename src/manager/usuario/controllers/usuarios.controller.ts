import { Body, Controller, Get, HttpCode, Logger, Post, Request } from '@nestjs/common';
import { ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { UsuariosService } from '../services/usuarios.service';
import { CreacionUsuarioRequest } from '../dto/creacion.usuario.request';

@ApiUseTags('Mantenimiento de Usuarios')
@Controller('v1/config/usuarios')
export class UsuariosController {
  constructor(private readonly service: UsuariosService) {
  }

  @ApiResponse({ status: 201, description: 'Creacion de campo.' })
  @Post()
  @HttpCode(201)
  public async create(@Body() record: CreacionUsuarioRequest) {
    return this.service.create(record, record.codbh);
  }

  @ApiResponse({ status: 200, description: 'Listado de usuarios.' })
  @Get()
  @HttpCode(200)
  public async get() {
    return this.service.find();
  }

  @Post('sincronizacion')
  public async sincronizacion(@Body() record: CreacionUsuarioRequest) {
    this.service.findByBhCode(record.codbh).then( exito => {
      return exito;
    }).catch(err => {
      return this.service.create(record, record.codbh);
    });
  }
}
