import { Module } from '@nestjs/common';
import { ConexionExternaController } from './controllers/conexion_externa/conexion.externa.controller';
import { ConexionExternaProvider } from './providers/conexion.externa.provider';
import { ConexionExternaService } from './services/conexion.externa.service';
import { DatabaseModule } from '../../database/database.module';
import { ListaModule } from '../lista/lista.module';
import { AuthModule } from '../../auth/auth.module';

@Module({
  imports: [DatabaseModule, ListaModule, AuthModule],
  controllers: [ConexionExternaController],
  providers: [...ConexionExternaProvider, ConexionExternaService],
  exports: [ConexionExternaService],
})
export class ConexionExternaModule {
}
