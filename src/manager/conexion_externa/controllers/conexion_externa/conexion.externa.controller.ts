import { Body, Controller, Delete, Get, Param, Post, Request, UseGuards } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';
import { ConexionExternaService } from '../../services/conexion.externa.service';
import { ConexionExternaRequest } from '../../dto/conexion.externa.request';
import { AuthGuard } from '@nestjs/passport';
import { UsuarioStrategy } from '../../../../auth/dtos/usuario.strategy';

@ApiUseTags('Configuracion de Conexiones de Listas Externas')
@UseGuards(AuthGuard('jwt'))
@Controller('/v1/config/conexion_externa')
export class ConexionExternaController {

  constructor(private readonly service: ConexionExternaService) {
  }

  @Get()
  public async findAll() {
    return await this.service.findAll();
  }

  @Post()
  public async setAsociar(@Request() request, @Body() mensaje: ConexionExternaRequest) {
    const infoToken: UsuarioStrategy = request.user;
    mensaje.createdBy = infoToken.usuario.codbh;
    return await this.service.create(mensaje);
  }

  @Delete(':id')
  public async delete(@Param() id) {
    return await this.service.delete(id);
  }
}
