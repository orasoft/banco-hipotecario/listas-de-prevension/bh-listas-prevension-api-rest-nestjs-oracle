import { Inject, Injectable, Logger } from '@nestjs/common';
import { Repository } from 'typeorm';
import { ConexionExterna } from '../../../database/entities/conexionExterna';
import { ConexionExternaRequest } from '../dto/conexion.externa.request';
import { ListasService } from '../../lista/services/listas.service';

@Injectable()
export class ConexionExternaService {
  constructor(@Inject('CONEXION_EXTERNA_REPOSITORY') private readonly repositorio: Repository<ConexionExterna>,
              private readonly listasService: ListasService) {
  }

  async findAll() {
    const [items, total] = await this.repositorio.findAndCount({ relations: ['lista'] });

    return {
      total,
      items,
    };
  }

  async findById(id: string) {
    return await this.repositorio.findOneOrFail(id);
  }

  async create(request: ConexionExternaRequest) {
    Logger.verbose(`request => ${JSON.stringify(request)}`);
    const lista = await this.listasService.findById(request.listaId);

    const conexion = new ConexionExterna();
    conexion.lista = lista;
    conexion.url = request.url;
    conexion.createdBy = request.createdBy;

    Logger.verbose(`Registro a Guardar => ${JSON.stringify(conexion)}`);

    return this.repositorio.manager.save(conexion);

    // const insertInfo = await this.repositorio.createQueryBuilder().insert().into(ConexionExterna).values({
    //   lista, url: request.url, createdBy: request.createdBy,
    // }).execute();
    //
    // const id = insertInfo.identifiers[0].id;
    // return this.repositorio.findOneOrFail(id);
  }

  async delete(id: string) {
    return await this.repositorio.delete(id);
  }

}
