import { Connection } from 'typeorm';
import { ConexionExterna } from '../../../database/entities/conexionExterna';

export const ConexionExternaProvider = [
  {
    provide: 'CONEXION_EXTERNA_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(ConexionExterna),
    inject: ['DATABASE_CONNECTION'],
  },
];
