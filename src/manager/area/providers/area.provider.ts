import { Connection } from 'typeorm';
import { Area } from '../../../database/entities/area';

export const AreaProvider = [
  {
    provide: 'AREA_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Area),
    inject: ['DATABASE_CONNECTION'],
  },
];
