import { ApiModelProperty } from '@nestjs/swagger';

export class EditarAreaResponsableRequest {
  @ApiModelProperty()
  areaId: string;

  @ApiModelProperty()
  responsableId: string;
}
