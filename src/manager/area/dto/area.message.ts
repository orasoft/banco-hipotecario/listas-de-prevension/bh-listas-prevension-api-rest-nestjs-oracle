import { AreaEstado } from '../../../database/entities';

export class AreaMessage {
  id?: string;
  nombre: string;
  estado: AreaEstado;
  creado: Date;
  actualizado: Date;
  version: number;
  responsables: any[];
  createdBy?: string;
  updatedBy?: string;
}
