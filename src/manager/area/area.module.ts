import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../database/database.module';
import { AreaProvider } from './providers/area.provider';
import { AreaService } from './services/area.service';
import { UsuarioModule } from '../usuario/usuario.module';
import { AreaController } from './controllers/area/area.controller';
import { AuthModule } from '../../auth/auth.module';

@Module({
  imports: [DatabaseModule, UsuarioModule, AuthModule],
  controllers: [AreaController],
  providers: [...AreaProvider, AreaService],
  exports: [AreaService],
})
export class AreaModule {
}
