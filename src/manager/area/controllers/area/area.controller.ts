import { Body, Controller, Delete, Get, Param, Request, Post, Put, UseGuards } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';
import { AreaService } from '../../services/area.service';
import { EditarAreaResponsableRequest } from '../../dto/editar.area.responsable.request';
import { AreaMessage } from '../../dto/area.message';
import { UsuarioStrategy } from '../../../../auth/dtos/usuario.strategy';
import { AuthGuard } from '@nestjs/passport';

@ApiUseTags('Configuracion de Areas')
@UseGuards(AuthGuard('jwt'))
@Controller('/v1/config/areas')
export class AreaController {

  constructor(private readonly service: AreaService) {
  }

  @Get()
  public async findAll() {
    return await this.service.findAll();
  }

  @Get(':id')
  public async findById(@Param('id') id: string) {
    return await this.service.findById(id);
  }

  @Post()
  public async crear(@Request() request, @Body() mensaje: Partial<AreaMessage>) {
    const user: UsuarioStrategy = request.user;
    mensaje.createdBy = user.usuario.codbh;
    return await this.service.create(mensaje);
  }

  @Put()
  public async updateNombre(@Request() request, @Body() mensaje: Partial<AreaMessage>) {
    const user: UsuarioStrategy = request.user;
    mensaje.updatedBy = user.usuario.codbh;
    return await this.service.update(mensaje);
  }

  @Post('responsable')
  public async setAsociar(@Body() request: EditarAreaResponsableRequest) {
    return await this.service.asociarResponsable(request);
  }

  @Delete('responsable')
  public async deleteResponsable(@Body() request: EditarAreaResponsableRequest) {
    return await this.service.deleteResponsable(request);
  }

  @Delete(':id')
  public async delete(@Request() request, @Param() id: string) {
    const user: UsuarioStrategy = request.user;
    return await this.service.delete(id, user.usuario.codbh);
  }
}
