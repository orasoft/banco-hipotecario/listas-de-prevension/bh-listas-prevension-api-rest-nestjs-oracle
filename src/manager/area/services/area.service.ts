import { Inject, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Area, AreaEstado, Usuario } from '../../../database/entities';
import { UsuariosService } from '../../usuario/services/usuarios.service';
import { EditarAreaResponsableRequest } from '../dto/editar.area.responsable.request';
import { AreaMessage } from '../dto/area.message';

@Injectable()
export class AreaService {
  constructor(@Inject('AREA_REPOSITORY') private readonly repositorio: Repository<Area>,
              private readonly responsableService: UsuariosService) {
  }

  public static setupRespuesta(area: Area): Partial<AreaMessage> {
    const respuesta = new AreaMessage();
    const responsables = [];

    area.responsables.forEach(responsable => {
      responsables.push(UsuariosService.setRespuesta(responsable));
    });

    respuesta.id = area.id;
    respuesta.nombre = area.nombre;
    respuesta.responsables = responsables;
    respuesta.actualizado = area.updatedAt;
    respuesta.creado = area.createdAt;
    respuesta.estado = area.estado;
    respuesta.version = area.version;
    return respuesta;
  }

  async findAll() {
    const [items, total] = await this.repositorio.findAndCount({ relations: ['responsables'], where: { estado: AreaEstado.activa } });
    const itemsRespuesta = [];

    items.forEach(item => {
      itemsRespuesta.push(AreaService.setupRespuesta(item));
    });

    return {
      total,
      items: itemsRespuesta,
    };
  }

  async findById(id: string) {
    const area = await this.repositorio.findOneOrFail(id, { relations: ['responsables'], where: { estado: AreaEstado.activa } });
    return AreaService.setupRespuesta(area);
  }

  async findByIdWithCache(id: string) {
    return await this.repositorio.findOneOrFail(id, { where: { estado: AreaEstado.activa } });
  }

  async create(request: Partial<AreaMessage>) {
    const insertInfo = await this.repositorio.createQueryBuilder().insert().into(Area).values([{
      nombre: request.nombre,
      estado: AreaEstado.activa,
      createdBy: request.createdBy,
    }]).execute();
    const id = insertInfo.identifiers[0].id;
    return await this.repositorio.findOneOrFail(id);
  }

  async update(request: Partial<AreaMessage>) {
    const area = await this.repositorio.findOneOrFail(request.id);
    area.nombre = request.nombre;
    area.updatedBy = request.updatedBy;

    // this.repositorio.createQueryBuilder().update().set(area).execute();

    return await this.repositorio.save(area);
  }

  async delete(id: string, updatedBy: string) {
    const area = await this.repositorio.findOneOrFail(id, { relations: ['responsables'] });
    area.estado = AreaEstado.desactivada;
    area.updatedBy = updatedBy;
    return await this.repositorio.save(area);
  }

  async asociarResponsable(request: EditarAreaResponsableRequest) {
    const area = await this.repositorio.findOneOrFail(request.areaId, { relations: ['responsables'] });
    const responsable = await this.responsableService.findById(request.responsableId);
    area.responsables.push(responsable);
    await this.repositorio.save(area);
    const areaActualizada = await this.repositorio.findOneOrFail(request.areaId, { relations: ['responsables'] });
    return AreaService.setupRespuesta(areaActualizada);
  }

  async deleteResponsable(request: EditarAreaResponsableRequest) {
    const area = await this.repositorio.findOneOrFail(request.areaId, { relations: ['responsables'] });
    area.responsables = Object.assign([], this.removeResponsable(request.responsableId, area.responsables));
    await this.repositorio.save(area);
    const areaActualizada = await this.repositorio.findOneOrFail(request.areaId, { relations: ['responsables'] });
    return AreaService.setupRespuesta(areaActualizada);
  }

  private removeResponsable(id: string, responsables: Usuario[]) {
    return responsables.filter(item => item.id !== id);
  }
}
