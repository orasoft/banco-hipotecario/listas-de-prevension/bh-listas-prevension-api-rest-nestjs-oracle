import { Injectable } from '@nestjs/common';
import { CampoSubcategoria } from '../../../database/entities';
import { SubcategoriaMessage } from '../dto/subcategoria.message';

@Injectable()
export class SubcategoriaService {
  static setupRespuesta(campoSubcategoria: CampoSubcategoria) {
    const respuesta = new SubcategoriaMessage();
    respuesta.id = campoSubcategoria.id;
    respuesta.label = campoSubcategoria.label;
    respuesta.valor = campoSubcategoria.valor;
    respuesta.version = campoSubcategoria.version;
    respuesta.createdAt = campoSubcategoria.createdAt;
    respuesta.updatedAt = campoSubcategoria.updatedAt;
    return respuesta;
  }
}
