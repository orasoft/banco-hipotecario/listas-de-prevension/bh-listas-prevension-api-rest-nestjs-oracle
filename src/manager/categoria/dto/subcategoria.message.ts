export class SubcategoriaMessage {
  id: string;
  label: string;
  valor: string;
  createdAt: Date;
  updatedAt: Date;
  version: number;
}
