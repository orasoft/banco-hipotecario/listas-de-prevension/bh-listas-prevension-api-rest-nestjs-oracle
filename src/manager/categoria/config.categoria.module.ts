import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../database/database.module';
import { SubcategoriaService } from './services/subcategoria.service';

@Module({
  imports: [DatabaseModule],
  providers: [SubcategoriaService],
  exports: [SubcategoriaService],
})
export class ConfigCategoriaModule {
}
