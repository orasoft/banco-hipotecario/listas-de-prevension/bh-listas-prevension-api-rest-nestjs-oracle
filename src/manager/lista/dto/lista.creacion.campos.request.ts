import { ApiModelProperty } from '@nestjs/swagger';
import { ListaCampoConfigEstado } from '../../../database/entities/listaCampoConfig';

export class ListaCreacionCamposRequest {
  @ApiModelProperty()
  campoId: string;

  @ApiModelProperty()
  requerido: boolean;

  @ApiModelProperty()
  busqueda: boolean;

  @ApiModelProperty()
  orden: number;

  @ApiModelProperty({ enum: ['A', 'I'] })
  estado: ListaCampoConfigEstado;
}
