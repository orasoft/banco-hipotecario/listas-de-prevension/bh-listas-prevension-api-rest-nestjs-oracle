import { ApiModelProperty } from '@nestjs/swagger';
import { EstadoLista, TipoLista } from '../../../database/entities';

export class ListaActualizarRequest {
  @ApiModelProperty()
  id: string;

  @ApiModelProperty()
  nombre: string;

  @ApiModelProperty()
  descripcion: string;

  @ApiModelProperty({ enum: ['Interna', 'Externa'] })
  tipo: TipoLista;

  @ApiModelProperty({ enum: ['A', 'I'] })
  estado: EstadoLista;

  @ApiModelProperty()
  criticidad: string;

  @ApiModelProperty({ isArray: true, type: String })
  responsables: string[];

  @ApiModelProperty()
  updatedBy: string;
}
