import { EstadoLista, TipoLista } from '../../../database/entities/lista';
import { ApiModelProperty } from '@nestjs/swagger';

export class ListaCreacionRequest {
  @ApiModelProperty()
  nombre: string;

  @ApiModelProperty()
  descripcion: string;

  @ApiModelProperty({ enum: ['Interna', 'Externa'] })
  tipo: TipoLista;

  @ApiModelProperty({ enum: ['A', 'I'] })
  estado: EstadoLista;

  @ApiModelProperty()
  criticidad: string;

  @ApiModelProperty({ isArray: true, type: String })
  responsables: string[];

  @ApiModelProperty()
  createdBy: string;
}
