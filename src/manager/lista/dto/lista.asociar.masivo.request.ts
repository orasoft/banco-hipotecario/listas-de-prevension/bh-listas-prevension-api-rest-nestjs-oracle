import { ApiModelProperty } from '@nestjs/swagger';
import { ListaCreacionCamposRequest } from './lista.creacion.campos.request';

export class ListaAsociarMasivoRequest {
  @ApiModelProperty()
  listaId: string;

  @ApiModelProperty({ type: ListaCreacionCamposRequest, isArray: true })
  campos: ListaCreacionCamposRequest[];
}
