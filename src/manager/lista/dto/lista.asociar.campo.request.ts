import { ApiModelProperty } from '@nestjs/swagger';
import { ListaCreacionCamposRequest } from './lista.creacion.campos.request';

export class ListaAsociarCampoRequest {
  @ApiModelProperty()
  listaId: string;

  @ApiModelProperty({ type: ListaCreacionCamposRequest })
  campo: ListaCreacionCamposRequest;
}
