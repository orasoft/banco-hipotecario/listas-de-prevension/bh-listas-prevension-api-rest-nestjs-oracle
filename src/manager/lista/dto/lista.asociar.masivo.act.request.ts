import { ApiModelProperty } from '@nestjs/swagger';
import { ListaActualizarCamposRequest } from './lista.actualizar.campos.request';

export class ListaAsociarMasivoActRequest {
  @ApiModelProperty()
  listaId: string;

  @ApiModelProperty({ type: ListaActualizarCamposRequest, isArray: true })
  campos: ListaActualizarCamposRequest[];
}
