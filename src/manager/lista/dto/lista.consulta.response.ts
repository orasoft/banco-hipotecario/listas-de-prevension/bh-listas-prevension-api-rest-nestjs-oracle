export class ListaConsultaResponse {
  id: string;
  descripcion: string;
  responsables: string[];
  ultimaCarga: string;
  estado: string;
  observaciones: string;
}
