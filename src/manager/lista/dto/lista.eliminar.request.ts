import { ApiModelProperty } from '@nestjs/swagger';

export class ListaEliminarRequest {
  @ApiModelProperty()
  justificacion: string;
}
