import { Module } from '@nestjs/common';
import { ListasProvider } from './providers/lista.provider';
import { ListasService } from './services/listas.service';
import { ListasController } from './controllers/listas.controller';
import { DatabaseModule } from '../../database/database.module';
import { ListaConfigModule } from '../lista_config/lista.config.module';
import { ConfigCampoModule } from '../campo/config.campo.module';
import { UsuarioModule } from '../usuario/usuario.module';
import { ConfigModule } from '../../config/config.module';
import { AuthModule } from '../../auth/auth.module';

@Module({
  providers: [...ListasProvider, ListasService, AuthModule],
  imports: [DatabaseModule, ListaConfigModule, ConfigCampoModule, UsuarioModule, ConfigModule],
  controllers: [ListasController],
  exports: [ListasService],
})
export class ListaModule {
}
