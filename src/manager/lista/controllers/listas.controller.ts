import { Body, Controller, Delete, Get, HttpCode, Logger, Param, Post, Put, Query, UseGuards, Request } from '@nestjs/common';
import { ListasService } from '../services/listas.service';
import { ListaAsociarMasivoRequest } from '../dto/lista.asociar.masivo.request';
import { ApiUseTags } from '@nestjs/swagger';

import { ListaAsociarCampoRequest } from '../dto/lista.asociar.campo.request';
import { ListaCreacionRequest } from '../dto/lista.creacion.request';
import { AuthGuard } from '@nestjs/passport';
import { UsuarioStrategy } from '../../../auth/dtos/usuario.strategy';
import { ListaEliminarRequest } from '../dto/lista.eliminar.request';
import { ListaActualizarRequest } from '../dto/lista.actualizar.request';
import { ListaAsociarMasivoActRequest } from '../dto/lista.asociar.masivo.act.request';

@ApiUseTags('Configuracion de Listas')
@UseGuards(AuthGuard('jwt'))
@Controller('v1/config/listas')
export class ListasController {
  constructor(private readonly service: ListasService) {
  }

  @Get('externas')
  public async findAllExternas() {
    return await this.service.findAllExternas();
  }

  @Get('inactivas')
  public async findAllInactivas() {
    return await this.service.findAllInactivas();
  }

  @Get('/consulta')
  public async findByName(@Query('listaId') listaId: string,
                          @Query('responsableId') responsableId: string,
                          @Query('fecha') fecha: string) {

    // Logger.log(`listaId => ${listaId}`);
    // Logger.log(`responsableId => ${responsableId}`);
    // Logger.log(`fecha => ${fecha}`);

    return await this.service.findByCustom(listaId, responsableId, fecha);
  }

  @Get('campos/orden/:id')
  public async findFieldConfigOrder(@Param('id') id: string) {
    return await this.service.findFieldsConfigOrder(id);
  }

  @Get('/campos/:id')
  public async findByIdCamposConfigurados(@Param('id') id) {
    return this.service.findByIdCamposConfigurados(id);
  }

  @Get('/perfiles/:id')
  public async findByIdPerfiles(@Param('id') id) {
    return this.service.findByIdPerfiles(id);
  }

  @Get(':id')
  public async findById(@Param('id') id: string) {
    return await this.service.findById(id);
  }

  @Get()
  public async findAll() {
    return await this.service.findAll();
  }

  @Post('/asociar')
  public async setAsociar(@Request() request, @Body() mensaje: ListaAsociarCampoRequest) {
    const infoToken: UsuarioStrategy = request.user;
    return await this.service.asociarCampo(mensaje.listaId, mensaje.campo, infoToken.usuario.createdBy );
  }

  @Post('/asociar/masivo')
  public async setAsociarMasivo(@Request() request, @Body() configuracion: ListaAsociarMasivoRequest) {
    const infoToken: UsuarioStrategy = request.user;
    await this.service.asociarCampoMasivo(configuracion, infoToken.usuario.createdBy);
    return {};
  }

  @Put('/asociar/masivo/act')
  public async setAsociarMasivoAct(@Request() request, @Body() configuracion: ListaAsociarMasivoActRequest) {
    const infoToken: UsuarioStrategy = request.user;
    await this.service.asociarCampoMasivoAct(configuracion, infoToken.usuario.updatedBy);
    return {};
  }

  @Post()
  @HttpCode(201)
  public async create(@Request() request, @Body() record: ListaCreacionRequest) {
    const infoToken: UsuarioStrategy = request.user;
    record.createdBy = infoToken.usuario.codbh;
    return await this.service.create(record);
  }

  @Put()
  public async updateLista(@Request() request, @Body() record: ListaActualizarRequest) {
    const infoToken: UsuarioStrategy = request.user;
    record.updatedBy = infoToken.usuario.codbh;
    return await this.service.updateList(record);
  }

  @Put('activar/:id')
  public async activarLista(@Request() request, @Param('id') id: string) {
    const infoToken: UsuarioStrategy = request.user;
    return await this.service.activarById(id, infoToken.usuario.codbh);
  }

  @Put('desactivar/:id')
  public async desactivarLista(@Request() request, @Param('id') id: string, @Body() mensaje: ListaEliminarRequest) {
    const infoToken: UsuarioStrategy = request.user;
    return await this.service.desactivarById(id, infoToken.usuario.codbh, mensaje);
  }

  @Delete(':id')
  public async delete(@Request() request, @Param('id') id, @Body() mensaje: ListaEliminarRequest) {
    const infoToken: UsuarioStrategy = request.user;
    return this.service.delete(id, infoToken.usuario.codbh, mensaje);
  }
}
