import { Connection } from 'typeorm';
import { Lista } from '../../../database/entities/lista';

export const ListasProvider = [
  {
    provide: 'LISTAS_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Lista),
    inject: ['DATABASE_CONNECTION'],
  },
];
