import { BadRequestException, Inject, Injectable, Logger } from '@nestjs/common';
import { Between, In, Repository } from 'typeorm';
import { EstadoLista, Lista, ListaCampoConfig, TipoLista, Usuario } from '../../../database/entities';
import { CamposService } from '../../campo/services/campos.service';
import { ListaConfigService } from '../../lista_config/services/lista.config.service';
import { ListaAsociarMasivoRequest } from '../dto/lista.asociar.masivo.request';
import { ListaCreacionRequest } from '../dto/lista.creacion.request';
import { ListaCreacionCamposRequest } from '../dto/lista.creacion.campos.request';
import { UsuariosService } from '../../usuario/services/usuarios.service';
import { addDays } from 'date-fns';
import { ConfigService } from '../../../config/config.service';
import moment = require('moment');
import { ListaEliminarRequest } from '../dto/lista.eliminar.request';
import { ListaActualizarRequest } from '../dto/lista.actualizar.request';
import { ListaAsociarMasivoActRequest } from '../dto/lista.asociar.masivo.act.request';
import { ListaActualizarCamposRequest } from '../dto/lista.actualizar.campos.request';

const StringBuilder = require('string-builder');

@Injectable()
export class ListasService {

  private relaciones = [
    'responsables',
  ];

  constructor(@Inject('LISTAS_REPOSITORY') private readonly repositorio: Repository<Lista>,
              private readonly camposService: CamposService,
              private readonly listaConfigService: ListaConfigService,
              private readonly usuarioService: UsuariosService,
              private readonly configService: ConfigService) {
  }

  static AfterDate = (date: Date) => Between(date, addDays(date, 1));

  async findAll() {
    const [items, total] = await this.repositorio.findAndCount({
      relations: ['responsables'],
      where: { estado: EstadoLista.activa },
    });

    return {
      total,
      items,
    };
  }

  async findAllExternas() {
    const [items, total] = await this.repositorio.findAndCount({
      select: ['id', 'nombre'],
      where: { tipo: TipoLista.externa, estado: EstadoLista.activa },
    });

    return {
      total,
      items,
    };
  }

  async findAllInactivas() {
    const [items, total] = await this.repositorio.findAndCount({ relations: this.relaciones, where: { estado: EstadoLista.inactiva } });

    return {
      total,
      items,
    };
  }

  async findById(id: string) {
    return await this.repositorio.findOneOrFail(id, {
      join: {
        alias: 'l',
        leftJoinAndSelect: {
          cc: 'l.camposConfigurados',
          c: 'cc.campo',
        },
      },
    });
  }

  async findByIdCamposConfigurados(id: string) {
    return await this.repositorio.findOneOrFail(id, {
        join: {
          alias: 'l',
          leftJoinAndSelect: {
            cc: 'l.camposConfigurados',
            c: 'cc.campo',
            subcat: 'c.subcategorias',
            r: 'l.responsables',
          },
        },
      },
    );
  }

  async findByIdPerfiles(id: string) {
    return await this.repositorio.findOneOrFail(id,
      {
        select: ['id', 'nombre'],
        relations: ['perfiles', 'perfiles.rol'],
        where: { estado: EstadoLista.activa },
        loadEagerRelations: true,
      });
  }

  async findFieldsConfigOrder(id: string) {
    const lista = await this.repositorio.findOneOrFail(id);
    return this.listaConfigService.findByLista(lista);
  }

  async findByIdWithCache(id: string) {
    return await this.repositorio.findOneOrFail(id, { relations: this.relaciones, cache: true });
  }

  // TODO - COMPLETADA - Que filtre por el responsable //
  async findByCustom(listaId: string = '*', responsableId: string = '*', fecha: string = '*') {

    const listaSubquery = new StringBuilder();
    const responsablesSubquery = new StringBuilder();
    let fechaIni = '';
    let fechaFin = '';
    if (listaId === '*') {
      listaSubquery.appendFormat(`select "id" from {0}."listas" where "estado" = 'A'`, this.configService.getConfigDatabase().SCHEMA);
    } else {
      listaSubquery.appendFormat(`select "id" from {0}."listas" where "id" = '{1}'`, this.configService.getConfigDatabase().SCHEMA, listaId);
    }

    if (responsableId === '*') {
      responsablesSubquery.appendFormat(`select b."id" from {0}."usuarios" b`, this.configService.getConfigDatabase().SCHEMA);
    } else {
      // tslint:disable-next-line:max-line-length
      responsablesSubquery.appendFormat(`select b."id" from {0}."usuarios" b where b."id" = '{1}'`, this.configService.getConfigDatabase().SCHEMA, responsableId);
    }

    if (fecha === '*') {
      fechaIni = moment('1900/01/01', 'YYYY-MM-DD').format('YYYY-MM-DD');
      fechaFin = moment(new Date()).add(1, 'days').format('YYYY-MM-DD');
    } else {
      fechaIni = moment(fecha, 'DD-MM-YYYY').format('YYYY-MM-DD');
      fechaFin = moment(fecha, 'DD-MM-YYYY').add(1, 'days').format('YYYY-MM-DD');
    }

    const consulta = new StringBuilder();
    // tslint:disable-next-line:max-line-length
    consulta.appendFormat(`select DISTINCT l."id" from {0}."listas" l left join {0}."listas_responsables" lr on l."id" = lr."listasId" left join {0}."usuarios" u on lr."usuariosId" = u."id" where l."id" in ( {1} ) and u."id" in ( {2} ) and l."updatedAt" between TO_DATE('{3}', 'YYYY-MM-DD') and TO_DATE('{4}', 'YYYY-MM-DD')`, this.configService.getConfigDatabase().SCHEMA, listaSubquery.toString(), responsablesSubquery.toString(), fechaIni, fechaFin);

    const listasEncontradas = await this.repositorio.query(consulta.toString());
    const listasEncontradasId = [];
    listasEncontradas.forEach((a) => listasEncontradasId.push(a.id));

    if (listasEncontradasId.length > 0) {
      const lista = await this.repositorio.find({
        where: {
          id: In(listasEncontradasId),
        },
      });

      const task = [];

      lista.forEach(async (a) => {
        task.push(this.getInfoCargaResponsables(a));
      });

      return Promise.all(task);
    } else {
      return [];
    }

  }

  async create(request: ListaCreacionRequest) {
    return this.procesarCreacion(request).then(async registro => {
      return this.repositorio.manager.save(registro).then(
        () => {
          const res = {
            code: 0,
            desc: 'Lista creada con éxito',
          };
          return res;
        }).catch(error => {
        let res;
        if (error.errorNum === 1) {
          res = {
            code: 1,
            desc: 'No se pudo crear la lista',
          };
        } else {
          res = {
            code: 99,
            desc: 'Error desconocido',
          };
        }
        return res;
      });

      // const insertInfo = await this.repositorio.createQueryBuilder().insert().into(Lista).values([{
      //   estado: registro.estado,
      //   descripcion: registro.descripcion,
      //   createdBy: registro.createdBy,
      //   nombre: registro.nombre,
      //   tipo: registro.tipo,
      //   criticidad: registro.criticidad,
      //   responsables: registro.responsables,
      // }]).execute();
      // return this.repositorio.findOneOrFail(insertInfo.identifiers[0].id);
    });
  }

  async updateList(lista: ListaActualizarRequest) {

    // const nuevaLista = {
    //   id: lista.id,
    //   nombre: lista.nombre,
    //   descripcion: lista.descripcion,
    //   tipo: lista.tipo,
    //   estado: lista.estado,
    //   criticidad: lista.criticidad,
    //   responsables: lista.responsables,
    //   updatedBy: lista.updatedBy,
    // };
    // Logger.log(`nuevaLista ====> ${nuevaLista}`)
    // return this.repositorio.manager.save(nuevaLista);

    return this.procesarActualizacion(lista).then(async registro => {
      return this.repositorio.manager.save(registro).then(
        () => {
          const res = {
            code: 0,
            desc: 'Lista actualizada con éxito',
          };
          return res;
        }).catch(error => {
        let res;
        if (error.errorNum === 1) {
          res = {
            code: 1,
            desc: 'No se pudo actualizar la lista',
          };
        } else {
          res = {
            code: 99,
            desc: 'Error desconocido',
          };
        }
        return res;
      });
    });
  }

  async update(id: string, registro: Partial<Lista>): Promise<Lista> {
    if (id === registro.id) {
      const recordFound = await this.repositorio.findOneOrFail(registro.id);
      // recordFound.busqueda = registro.busqueda;
      recordFound.nombre = registro.nombre;
      recordFound.descripcion = registro.descripcion;
      recordFound.estado = registro.estado;
      recordFound.tipo = registro.tipo;

      return await this.repositorio.manager.save(recordFound).then(record => {
        return record;
      });
    }
    throw new BadRequestException();
  }

  async activarById(id: string, updatedBy: string) {
    const lista = await this.repositorio.findOneOrFail(id);
    lista.estado = EstadoLista.activa;
    lista.updatedBy = updatedBy;
    await this.repositorio.manager.save(lista);
    return await this.findById(id);
  }

  async desactivarById(id: string, updatedBy: string, mensaje: ListaEliminarRequest) {
    const lista = await this.repositorio.findOneOrFail(id);
    lista.estado = EstadoLista.inactiva;
    lista.updatedBy = updatedBy;
    lista.observaciones = mensaje.justificacion;
    await this.repositorio.manager.save(lista);
    return await this.findById(id);
  }

  async delete(id: string, updatedBy: string, mensaje: ListaEliminarRequest) {
    return await this.desactivarById(id, updatedBy, mensaje);
  }

  async asociarCampo(listaId: string, campoAsociar: ListaCreacionCamposRequest, createdBy: string) {
    const lista = await this.repositorio.findOneOrFail(listaId);
    const campo = await this.camposService.findById(campoAsociar.campoId);

    const record: ListaCampoConfig = new ListaCampoConfig();
    record.lista = lista;
    record.campo = campo;
    record.requerido = campoAsociar.requerido;
    record.busqueda = campoAsociar.busqueda;
    record.orden = campoAsociar.orden;
    record.estado = campoAsociar.estado;
    record.createdBy = createdBy;
    this.listaConfigService.create(record);
  }

  async asociarCampoMasivo(configuracion: ListaAsociarMasivoRequest, createdBy: string) {
    const listaId = configuracion.listaId;
    await configuracion.campos.forEach((record) => {
      this.asociarCampo(listaId, record, createdBy);
    });
  }

  async asociarCampoMasivoAct(configuracion: ListaAsociarMasivoActRequest, updatedBy: string) {
    const listaId = configuracion.listaId;
    await configuracion.campos.forEach((record) => {
      this.asociarCampoAct(listaId, record, updatedBy);
    });
  }

  async asociarCampoAct(listaId: string, campoAsociar: ListaActualizarCamposRequest, createdBy: string) {
    const lista = await this.repositorio.findOneOrFail(listaId);
    const campo = await this.camposService.findById(campoAsociar.campoId);

    const record: ListaCampoConfig = new ListaCampoConfig();
    record.id = campoAsociar.id;
    record.lista = lista;
    record.campo = campo;
    record.requerido = campoAsociar.requerido;
    record.busqueda = campoAsociar.busqueda;
    record.orden = campoAsociar.orden;
    record.estado = campoAsociar.estado;
    record.createdBy = createdBy;
    this.listaConfigService.create(record);
  }

  private async getInfoCargaResponsables(record: any) {
    return new Promise(async (resolve) => {
      const queryUltimaCarga = new StringBuilder();
      // tslint:disable-next-line:max-line-length
      queryUltimaCarga.appendFormat(`select cm."updatedAt" from {0}."cargas_masivas" cm where cm."listaId" = '{1}' order by cm."updatedAt" DESC FETCH NEXT 1 ROWS ONLY`, this.configService.getConfigDatabase().SCHEMA, record.id);
      const ultimaCarga = await this.repositorio.query(queryUltimaCarga.toString());
      const queryResponsables = new StringBuilder();
      // tslint:disable-next-line:max-line-length
      queryResponsables.appendFormat(`select u."id", u."usuario" from {0}."listas" l left join {0}."listas_responsables" lr on l."id" = lr."listasId" left join {0}."usuarios" u on lr."usuariosId" = u."id" where l."id" = '{1}'`, this.configService.getConfigDatabase().SCHEMA, record.id);
      const responsables = await this.repositorio.query(queryResponsables.toString());
      const responsablesList = [];
      responsables.forEach(a => {
        responsablesList.push({
            id: a.id,
            resp: a.usuario,
          });
      });

      return resolve({
        id: record.id,
        nombre: record.nombre,
        descripcion: record.descripcion,
        estado: record.estado,
        observaciones: '',
        responsables: responsablesList,
        ultimaCarga: ultimaCarga.length > 0 ? ultimaCarga[0].updatedAt : '',
      });
    });
  }

  private procesarCreacion(request: ListaCreacionRequest): Promise<Lista> {
    return new Promise((resolve, reject) => {
      const colaPeticiones = [];
      request.responsables.forEach(responsableId => {
        colaPeticiones.push(this.buscarResponsable(responsableId));
      });

      const responsables = Promise.all(colaPeticiones);

      responsables.then(resp => {
        const registro = new Lista();

        registro.nombre = request.nombre;
        registro.tipo = request.tipo;
        registro.estado = request.estado;
        registro.criticidad = request.criticidad;
        registro.descripcion = request.descripcion;
        registro.responsables = resp;
        registro.createdBy = request.createdBy;
        resolve(registro);
      });
    });
  }

  private procesarActualizacion(request: ListaActualizarRequest): Promise<Lista> {
    return new Promise((resolve, reject) => {
      const colaPeticiones = [];
      request.responsables.forEach(responsableId => {
        colaPeticiones.push(this.buscarResponsable(responsableId));
      });

      const responsables = Promise.all(colaPeticiones);

      responsables.then(resp => {
        const registro = new Lista();

        registro.id = request.id;
        registro.nombre = request.nombre;
        registro.tipo = request.tipo;
        registro.estado = request.estado;
        registro.criticidad = request.criticidad;
        registro.descripcion = request.descripcion;
        registro.responsables = resp;
        registro.updatedBy = request.updatedBy;
        resolve(registro);
      });
    });
  }

  private buscarResponsable(responsableId: string): Promise<Usuario> {
    return new Promise((resolve) => {
      return this.usuarioService.findById(responsableId).then(record => {
        return resolve(record);
      });
    });
  }
}
