import { Inject, Injectable, Logger } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Lista, ListaCampoConfig } from '../../../database/entities';

@Injectable()
export class ListaConfigService {
  constructor(@Inject('LISTAS_CAMPO_CONFIG_REPOSITORY') private readonly repositorio: Repository<ListaCampoConfig>) {
  }

  async create(registro: ListaCampoConfig): Promise<ListaCampoConfig> {
    Logger.verbose(`Registro a guardar => ${JSON.stringify(registro)}`);
    return await this.repositorio.manager.save(registro);
  }

  async delete(id: string) {
    return await this.repositorio.delete(id).then(record => {
      return record;
    });
  }

  async findByLista(lista: Lista) {
    return await this.repositorio.find({ select: ['id', 'orden'], where: { lista }, order: { orden: 'ASC' } });
  }
}
