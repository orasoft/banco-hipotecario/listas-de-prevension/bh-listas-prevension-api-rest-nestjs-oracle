import { Module } from '@nestjs/common';
import { ListasCampoConfigProvider } from './providers/lista.config.provider';
import { ListaConfigService } from './services/lista.config.service';
import { DatabaseModule } from '../../database/database.module';
import { AuthModule } from '../../auth/auth.module';

@Module({
  providers: [...ListasCampoConfigProvider, ListaConfigService, AuthModule],
  imports: [DatabaseModule],
  exports: [ListaConfigService],
})
export class ListaConfigModule {
}
