import { Connection } from 'typeorm';
import { ListaCampoConfig } from '../../../database/entities/listaCampoConfig';

export const ListasCampoConfigProvider = [
  {
    provide: 'LISTAS_CAMPO_CONFIG_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(ListaCampoConfig),
    inject: ['DATABASE_CONNECTION'],
  },
];
