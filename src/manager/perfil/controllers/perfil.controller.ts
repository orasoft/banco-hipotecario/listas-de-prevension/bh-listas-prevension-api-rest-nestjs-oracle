import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put, Request, Query, UseGuards, Logger } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';
import { CreacionPerfilRequest } from '../dto/creacion.perfil.request';
import { PerfilesService } from '../services/perfiles.service';
import { Perfil } from '../../../database/entities';
import { CampoPerfilRequest } from '../dto/campoPerfilRequest';
import { AuthGuard } from '@nestjs/passport';
import { UsuarioStrategy } from '../../../auth/dtos/usuario.strategy';

@ApiUseTags('Mantenimiento de Perfiles')
@UseGuards(AuthGuard('jwt'))
@Controller('v1/config/perfiles')
export class PerfilController {

  constructor(private readonly service: PerfilesService) {
  }

  @Get('/consulta')
  @HttpCode(200)
  public async findByListaAndRol(@Query('listaId') listaId: string, @Query('rolId') rolId: string) {
    return this.service.findByListaAndRol(listaId, rolId);
  }

  @Get(':id/campos')
  public async findById(@Param('id') id: string) {
    return this.service.findById(id);
  }

  @Post()
  @HttpCode(201)
  public async create(@Request() request, @Body() record: CreacionPerfilRequest) {
    const infoToken: UsuarioStrategy = request.user;
    record.createdBy = infoToken.usuario.codbh;
    return this.service.create(record);
  }

  @Put()
  public async update(@Request() request, @Body() record: Perfil) {
    const infoToken: UsuarioStrategy = request.user;
    record.updatedBy = infoToken.usuario.codbh;
    return this.service.update(record);
  }

  @Post('/agregar/campo')
  public async agregarCampo(@Body() request: CampoPerfilRequest) {
    return await this.service.agregarCampo(request);
  }

  @Delete('/eliminar/campo')
  public async eliminarCampo(@Body() request: CampoPerfilRequest) {
    return await this.service.eliminarCampo(request);
  }
}
