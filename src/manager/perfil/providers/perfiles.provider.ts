import { Connection } from 'typeorm';
import { Perfil } from '../../../database/entities/perfil';

export const PerfilesProvider = [
  {
    provide: 'PERFILES_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Perfil),
    inject: ['DATABASE_CONNECTION'],
  },
];
