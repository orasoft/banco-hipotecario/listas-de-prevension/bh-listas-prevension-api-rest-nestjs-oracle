import { PerfilAcceso, PerfilVisualizar } from '../../../database/entities/perfil';
import { ApiModelProperty } from '@nestjs/swagger';

export class CreacionPerfilRequest {
  @ApiModelProperty()
  id?: string;

  @ApiModelProperty({ type: PerfilAcceso, enum: ['Consulta', 'Edicion', 'Ninguno'] })
  acceso: PerfilAcceso;

  @ApiModelProperty({ type: PerfilVisualizar, enum: ['Vigente', 'Historico'] })
  visualizar: PerfilVisualizar;

  @ApiModelProperty()
  descargar: boolean;

  @ApiModelProperty()
  responsable: boolean;

  @ApiModelProperty()
  listaId: string;

  @ApiModelProperty()
  rolId: string;

  @ApiModelProperty({ type: String, isArray: true })
  campos: string[];

  @ApiModelProperty()
  createdBy: string;

  @ApiModelProperty()
  updateBy: string;
}
