import { Inject, Logger } from '@nestjs/common';
import { Repository } from 'typeorm';
import { Campo, Perfil } from '../../../database/entities';
import { CreacionPerfilRequest } from '../dto/creacion.perfil.request';
import { RolesService } from '../../rol/services/roles.service';
import { ListasService } from '../../lista/services/listas.service';
import { CamposService } from '../../campo/services/campos.service';
import { CampoPerfilRequest } from '../dto/campoPerfilRequest';

export class PerfilesService {
  constructor(@Inject('PERFILES_REPOSITORY') private readonly repositorio: Repository<Perfil>,
              private readonly rolesService: RolesService,
              private readonly listasService: ListasService,
              private readonly camposService: CamposService,
  ) {
  }

  async findById(id: string) {
    return await this.repositorio.findOneOrFail(id, { relations: ['campos', 'campos.subcategorias'] });
  }

  async findByListaAndRol(listaId: string, rolId: string) {
    return this.repositorio.manager.find(Perfil, {
      join: {
        alias: 'perfil',
        leftJoin: {
          lista: 'perfil.lista',
          rol: 'perfil.rol',
        },
      },
      where: {
        lista: {
          id: listaId,
        },
        rol: {
          id: rolId,
        },
      },
    });
  }

  async create(request: CreacionPerfilRequest): Promise<Perfil> {
    const registro = await this.asignacion(request);

    return this.repositorio.save(registro).then(record => {
      return record;
    });
  }

  async update(perfil: Perfil) {
    return this.repositorio.save(perfil);
  }

  async delete(id: string) {
    return await this.repositorio.delete(id).then(record => {
      return record;
    });
  }

  async agregarCampo(request: CampoPerfilRequest) {
    const perfil = await this.repositorio.findOneOrFail(request.perfilId, { relations: ['campos'] });
    const campo = await this.camposService.findById(request.campoId);
    perfil.campos.push(campo);
    return this.repositorio.save(perfil);
  }

  async eliminarCampo(request: CampoPerfilRequest) {
    const perfil = await this.repositorio.findOneOrFail(request.perfilId, { relations: ['campos'] });
    const campo = await this.camposService.findById(request.campoId);
    perfil.campos = Object.assign([], perfil.campos.filter(a => a.id !== request.campoId));
    return this.repositorio.save(perfil);
  }

  private async asignacion(request: CreacionPerfilRequest): Promise<Perfil> {
    const rol = await this.rolesService.findById(request.rolId);
    const lista = await this.listasService.findById(request.listaId);
    const campos = await this.procesarCreacion(request);

    const registro = new Perfil();
    registro.id = request.id;
    registro.acceso = request.acceso;
    registro.descargar = request.descargar;
    registro.visualizar = request.visualizar;
    registro.responsable = request.responsable;
    registro.createdBy = request.createdBy;
    registro.lista = lista;
    registro.rol = rol;
    registro.campos = campos;

    return registro;
  }

  private procesarCreacion(request: CreacionPerfilRequest): Promise<Campo[]> {
    return new Promise(resolve => {
      const colaPeticiones = [];
      request.campos.forEach(campoId => {
        colaPeticiones.push(this.buscarCampo(campoId));
      });

      const campos = Promise.all(colaPeticiones);

      resolve(campos);
    });
  }

  private buscarCampo(campoId: string) {
    return new Promise(resolve => {
      return this.camposService.findById(campoId).then(record => {
        resolve(record);
      });
    });
  }
}
