import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../database/database.module';
import { PerfilController } from './controllers/perfil.controller';
import { PerfilesProvider } from './providers/perfiles.provider';
import { PerfilesService } from './services/perfiles.service';
import { RolesModule } from '../rol/roles.module';
import { ListaModule } from '../lista/lista.module';
import { ConfigCampoModule } from '../campo/config.campo.module';
import { AuthModule } from '../../auth/auth.module';

@Module({
  imports: [DatabaseModule, RolesModule, ListaModule, ConfigCampoModule, AuthModule],
  controllers: [PerfilController],
  providers: [...PerfilesProvider, PerfilesService],
  exports: [PerfilesService],
})
export class PerfilesModule {
}
