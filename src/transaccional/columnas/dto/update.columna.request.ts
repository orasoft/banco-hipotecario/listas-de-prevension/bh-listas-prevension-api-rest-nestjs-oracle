import { ApiModelProperty } from '@nestjs/swagger';

export class UpdateColumnaRequest {

  @ApiModelProperty()
  id: string;

  @ApiModelProperty()
  valor: string;

  @ApiModelProperty()
  updatedBy?: string;
}
