import { ApiModelProperty } from '@nestjs/swagger';

export class CreacionColumnaRequest {

  @ApiModelProperty()
  campoId: string;

  @ApiModelProperty()
  transaccionId: string;

  @ApiModelProperty()
  valor: string;

  @ApiModelProperty()
  createdBy: string;
}
