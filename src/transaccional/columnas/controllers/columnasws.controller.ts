import { ApiUseTags } from '@nestjs/swagger';
import { Body, Controller, Post, UseGuards, Request, Put, Get, Param } from '@nestjs/common';
import { ColumnaService } from '../services/columna.service';
import { CreacionColumnaRequest } from '../dto/creacion.columna.request';
import { AuthGuard } from '@nestjs/passport';
import { UsuarioStrategy } from '../../../auth/dtos/usuario.strategy';
import { request } from 'http';
import { UpdateColumnaRequest } from '../dto/update.columna.request';

@UseGuards(AuthGuard('jwt'))
@Controller('v1/webservice')
export class ColumnaswsController {

  constructor(private readonly service: ColumnaService) {}

  @Get('find/:id')
  async transaccionesById(@Param('id') trxId: string) {
    return await this.service.findByTrxId(trxId);
  }
}
