import { ApiUseTags } from '@nestjs/swagger';
import { Body, Controller, Post, UseGuards, Request, Put, Get, Param } from '@nestjs/common';
import { ColumnaService } from '../services/columna.service';
import { CreacionColumnaRequest } from '../dto/creacion.columna.request';
import { AuthGuard } from '@nestjs/passport';
import { UsuarioStrategy } from '../../../auth/dtos/usuario.strategy';
import { request } from 'http';
import { UpdateColumnaRequest } from '../dto/update.columna.request';

@ApiUseTags('Columnas de las transacciones')
@UseGuards(AuthGuard('jwt'))
@Controller('/v1/transaccional/columnas')
export class ColumnasController {

  constructor(private readonly service: ColumnaService) {}

  @Post()
  async create(@Request() request, @Body() mensaje: CreacionColumnaRequest) {
    const infoToken: UsuarioStrategy = request.user;
    mensaje.createdBy = infoToken.usuario.codbh;
    return await this.service.createQueryBuilder(mensaje);
  }

  @Put()
  async put(@Request() request, @Body() mensaje: UpdateColumnaRequest) {
    const infoToken: UsuarioStrategy = request.user;
    mensaje.updatedBy = infoToken.usuario.codbh;
    return this.service.updateById(mensaje);
  }

  @Get(':id')
  async transaccionesById(@Param('id') trxId: string) {
    return await this.service.findByTrxId(trxId);
  }
}
