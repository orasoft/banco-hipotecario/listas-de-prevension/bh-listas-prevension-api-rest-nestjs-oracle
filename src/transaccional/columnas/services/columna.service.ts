import { Inject, Injectable } from '@nestjs/common';
import { Repository } from 'typeorm';
import { CreacionColumnaRequest } from '../dto/creacion.columna.request';
import { TransaccionDetalle } from '../../../database/entities';
import { ConfigService } from '../../../config/config.service';
import { UpdateColumnaRequest } from '../dto/update.columna.request';
import { ResultBusquedaDatabase } from '../../transacciones/dto/result.busqueda.database';
import { TransaccionResponse } from '../../transacciones/dto/transaccion.response';

const StringBuilder = require('string-builder');
const uuidv4 = require('uuid/v4');

@Injectable()
export class ColumnaService {
  constructor(@Inject('COLUMNAS_REPOSITORY') private readonly repositorio: Repository<TransaccionDetalle>,
              private readonly configService: ConfigService) {
  }

  async createQueryBuilder(request: CreacionColumnaRequest) {
    const querySentence = new StringBuilder();

    // tslint:disable-next-line:max-line-length
    querySentence.appendFormat(`INSERT INTO {0}."transacciones_detalle" ("id", "valor", "campoId", "transaccionId", "createdBy") VALUES ('{1}','{2}','{3}','{4}', '{5}')`,
      this.configService.getConfigDatabase().SCHEMA, uuidv4(), request.valor, request.campoId, request.transaccionId, request.createdBy);
    return this.repositorio.query(querySentence.toString());
  }

  async findById(id: string) {
    return await this.repositorio.findOneOrFail(id);
  }

  async updateById(mensaje: UpdateColumnaRequest) {
    const record = await this.repositorio.findOneOrFail(mensaje.id);
    record.valor = mensaje.valor;
    record.updatedBy = mensaje.updatedBy;
    this.repositorio.manager.save(record);
    return record;
  }

  async findByTrxId(id) {
    // const querySentence = new StringBuilder();
    //
    // // tslint:disable-next-line:max-line-length
    // querySentence.appendFormat(`SELECT "id", "valor", "campoId", "transaccionId", "createdAt", "createdBy", "updatedAt", "updatedBy"  FROM {0}."transacciones_detalle" where "transaccionId" = '{1}'`, this.configService.getConfigDatabase().SCHEMA, id);
    // const response = {
    //   trxId: id,
    //   fields: await this.repositorio.query(querySentence.toString()),
    // }
    // return response;

    let resultado: ResultBusquedaDatabase[];
    let info: any;
    if (1) {
      const queryResultado = `SELECT TRXS."id" "TRXID",
        LT."id" "LTID",
        LT."nombre" "LTNOMBRE",
        TRXSD."id" "TRXDID",
        (select "U"."usuario" from MIS."listas_responsables" LR
            inner join MIS."usuarios" U on LR."usuariosId" = U."id" where LR."listasId" = LT."id" FETCH FIRST 1 ROWS ONLY) "RESPONSABLE",
        CM."forma" "CMFORMA",
        TRXSD."campoId" "CAMPOID",
        TRXSD."valor" "VALOR"
 FROM MIS."transacciones" "TRXS"
         LEFT JOIN MIS."transacciones_detalle" "TRXSD" ON TRXS."id" = TRXSD."transaccionId"
          LEFT JOIN MIS."campos" "CAMPOS" ON CAMPOS."id" = TRXSD."campoId"
          LEFT JOIN MIS."cargas_masivas" "CM" ON CM."id" = TRXS."cargaMasivaId"
         LEFT JOIN MIS."listas" "LT" ON LT."id" = CM."listaId"
WHERE TRXS."id" = '${id}' ORDER BY TRXS."id"`;

      resultado = await this.repositorio.query(queryResultado);

      let transaccion: TransaccionResponse = new TransaccionResponse();
      const transaccionesResponse: TransaccionResponse[] = [];

      resultado.forEach(a => {
        if (transaccion.id !== a.TRXID) {
          if (transaccion.id !== undefined) {
            transaccionesResponse.push(transaccion);
            transaccion = new TransaccionResponse();
          }
        }
        // Logger.error(`Id => ${a.TRXID}`)
        transaccion.id = a.TRXID;
        transaccion.listaId = a.LTID;
        // transaccion.responsable = a.RESPONSABLE;
        transaccion.lista = a.LTNOMBRE;
        // transaccion.tipo = a.CMFORMA;
        transaccion.campos.push({ id: a.CAMPOID, valor: a.VALOR });
      });
      if (resultado.length > 0) {
        transaccionesResponse.push(transaccion);
      }

      const queryInfo = new StringBuilder();
      // tslint:disable-next-line:max-line-length
      queryInfo.appendFormat(`select rownum as "orden", "campoId", "label" from ( SELECT "c"."id" AS "campoId", "c"."label" AS "label" FROM {0}."campos" "c" WHERE "c"."default" = 1 order by "c"."createdAt")`, this.configService.getConfigDatabase().SCHEMA);
      info = await this.repositorio.query(queryInfo.toString());

      return {
        header: info,
        registro: transaccionesResponse,
      };
    } else {
      return {
        header: [],
        registro: [],
      };
    }

  }
}
