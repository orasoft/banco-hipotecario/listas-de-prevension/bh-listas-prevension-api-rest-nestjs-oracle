import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../database/database.module';
import { ColumnaProvider } from './providers/columna.provider';
import { ColumnaService } from './services/columna.service';
import { ColumnasController } from './controllers/columnas.controller';
import { ConfigCampoModule } from '../../manager/campo/config.campo.module';
import { TransaccionesModule } from '../transacciones/transacciones.module';
import { ConfigModule } from '../../config/config.module';
import { AuthModule } from '../../auth/auth.module';
import { ColumnaswsController } from './controllers/columnasws.controller';

@Module({
  imports: [DatabaseModule, ConfigCampoModule, TransaccionesModule, ConfigModule, AuthModule],
  providers: [...ColumnaProvider, ColumnaService],
  exports: [ColumnaService],
  controllers: [ColumnasController, ColumnaswsController],
})
export class ColumnasModule {
}
