import { Connection } from 'typeorm';
import { TransaccionDetalle } from '../../../database/entities/transaccionDetalle';

export const ColumnaProvider = [
  {
    provide: 'COLUMNAS_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(TransaccionDetalle),
    inject: ['DATABASE_CONNECTION'],
  },
];
