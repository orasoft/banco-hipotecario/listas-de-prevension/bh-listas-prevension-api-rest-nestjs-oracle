import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../database/database.module';
import { CargaMasivaProvider } from './providers/carga.masiva.provider';
import { ListaModule } from '../../manager/lista/lista.module';
import { UsuarioModule } from '../../manager/usuario/usuario.module';
import { AreaModule } from '../../manager/area/area.module';
import { CargaMasivaService } from './services/carga.masiva.service';
import { CargaMasivaController } from './controllers/carga.masiva.controller';
import { AuthModule } from '../../auth/auth.module';

@Module({
  imports: [DatabaseModule, ListaModule, UsuarioModule, AreaModule, AuthModule],
  providers: [...CargaMasivaProvider, CargaMasivaService],
  exports: [CargaMasivaService],
  controllers: [CargaMasivaController],
})
export class CargaMasivaModule {
}
