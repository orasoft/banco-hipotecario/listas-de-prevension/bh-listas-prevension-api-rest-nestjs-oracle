import { Inject, Injectable, Logger } from '@nestjs/common';
import { Repository } from 'typeorm';
import { CargaMasiva, EstadoCargaMasiva } from '../../../database/entities';
import { CreacionCargaMasivaRequest } from '../dto/creacion.carga.masiva.request';
import { UsuariosService } from '../../../manager/usuario/services/usuarios.service';
import { ListasService } from '../../../manager/lista/services/listas.service';
import { AreaService } from '../../../manager/area/services/area.service';
import { UpdateCargaMasivaRequest } from '../dto/update.carga.masiva.request';

@Injectable()
export class CargaMasivaService {
  constructor(@Inject('CARGA_MASIVA_REPOSITORY') private readonly repositorio: Repository<CargaMasiva>,
              private readonly usuariosService: UsuariosService,
              private readonly listasService: ListasService,
              private readonly areasService: AreaService,
  ) {
  }

  async create(request: CreacionCargaMasivaRequest) {
    const usuario = await this.usuariosService.findByIdWithCache(request.usuarioId);
    const lista = await this.listasService.findByIdWithCache(request.listaId);
    const area = await this.areasService.findByIdWithCache(request.areaId);

    const cargaMasiva = new CargaMasiva();
    cargaMasiva.area = area;
    cargaMasiva.lista = lista;
    cargaMasiva.usuario = usuario;
    cargaMasiva.forma = request.forma;
    cargaMasiva.createdBy = request.createdBy;
    cargaMasiva.estado = EstadoCargaMasiva.activa;

    return this.repositorio.manager.save(cargaMasiva);

    // const insertInfo = await this.repositorio.createQueryBuilder().insert().into(CargaMasiva).values([{
    //   createdBy: request.createdBy,
    //   area,
    //   lista,
    //   usuario,
    //   forma: request.forma,
    //   estado: EstadoCargaMasiva.activa,
    // }]).execute();
    // // const id = insertInfo.identifiers[0].id;
    // // this.repositorio.findOneOrFail(id, { select: ['id'] });
    // return insertInfo.identifiers[0].id;
  }

  async findAll() {
    const consulta = await this.repositorio.find({ order: { updatedAt: 'DESC' }, relations: ['area', 'usuario', 'transacciones'] });
    const carga = [];
    consulta.forEach(a => {
      carga.push({
        id: a.id,
        estado: a.estado,
        motivo: a.motivo,
        solitadoPor: a.area.nombre,
        cargadoPor: a.usuario.usuario,
        fechaCarga: a.createdAt,
        total: a.transacciones.length,
      });
    });

    return carga;
  }

  async findById(id: string) {
    // Logger.log(`Carga a buscar ==> ${id}`);
    const carga = await this.repositorio.findOneOrFail(id);
    // Logger.log(`Carga encontrada ==> ${JSON.stringify(carga)}`);
    return carga;
  }

  async downById(id: string, request: UpdateCargaMasivaRequest) {
    const carga = await this.repositorio.findOneOrFail(id);
    carga.estado = EstadoCargaMasiva.desactivada;
    carga.motivo = request.motivo;
    carga.updatedBy = request.updatedBy;

    await this.repositorio.manager.save(carga);
    return this.repositorio.findOneOrFail(carga.id);
  }
}
