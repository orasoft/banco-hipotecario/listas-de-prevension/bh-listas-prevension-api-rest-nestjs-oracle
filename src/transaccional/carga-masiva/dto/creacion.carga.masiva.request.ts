import { ApiModelProperty } from '@nestjs/swagger';
import { FormaCarga } from '../../../database/entities';

export class CreacionCargaMasivaRequest {

  @ApiModelProperty()
  areaId: string;

  @ApiModelProperty()
  usuarioId: string;

  @ApiModelProperty()
  listaId: string;

  @ApiModelProperty({ enum: FormaCarga })
  forma: FormaCarga;

  @ApiModelProperty()
  createdBy: string;
}
