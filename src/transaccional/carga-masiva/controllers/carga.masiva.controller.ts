import { ApiUseTags } from '@nestjs/swagger';
import { Body, Controller, Post, Get, Put, Param, Request, Logger, UseGuards } from '@nestjs/common';
import { CreacionCargaMasivaRequest } from '../dto/creacion.carga.masiva.request';
import { CargaMasivaService } from '../services/carga.masiva.service';
import { UpdateCargaMasivaRequest } from '../dto/update.carga.masiva.request';
import { UsuarioStrategy } from '../../../auth/dtos/usuario.strategy';
import { AuthGuard } from '@nestjs/passport';

@ApiUseTags('Carga Masiva de Datos')
@UseGuards(AuthGuard('jwt'))
@Controller('/v1/transaccional/carga')
export class CargaMasivaController {

  constructor(private readonly service: CargaMasivaService) {
  }

  @Get()
  async getAll() {
    return await this.service.findAll();
  }

  @Post()
  async create(@Request() request, @Body() mensaje: CreacionCargaMasivaRequest) {
    const tokenInfo: UsuarioStrategy = request.user;
    mensaje.createdBy = tokenInfo.usuario.codbh;
    return await this.service.create(mensaje);
  }

  @Put(':id')
  async down(@Request() request, @Param('id') cargaId: string, @Body() mensaje: UpdateCargaMasivaRequest) {
    const tokenInfo: UsuarioStrategy = request.user;
    mensaje.updatedBy = tokenInfo.usuario.codbh;
    return await this.service.downById(cargaId, mensaje);
  }
}
