import { Connection } from 'typeorm';
import { CargaMasiva } from '../../../database/entities/cargaMasiva';

export const CargaMasivaProvider = [
  {
    provide: 'CARGA_MASIVA_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(CargaMasiva),
    inject: ['DATABASE_CONNECTION'],
  },
];
