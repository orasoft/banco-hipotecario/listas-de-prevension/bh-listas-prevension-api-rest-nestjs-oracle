import { Connection } from 'typeorm';
import { Transaccion } from '../../../database/entities/transaccion';

export const TransaccionProvider = [
  {
    provide: 'TRANSACCIONES_REPOSITORY',
    useFactory: (connection: Connection) => connection.getRepository(Transaccion),
    inject: ['DATABASE_CONNECTION'],
  },
];
