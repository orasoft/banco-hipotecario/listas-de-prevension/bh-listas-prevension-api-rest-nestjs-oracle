import { Module } from '@nestjs/common';
import { DatabaseModule } from '../../database/database.module';
import { TransaccionProvider } from './providers/transaccion.provider';
import { TransaccionService } from './services/transaccion.service';
import { TransaccionController } from './controllers/transaccion.controller';
import { CargaMasivaModule } from '../carga-masiva/carga.masiva.module';
import { ConfigModule } from '../../config/config.module';
import { AuthModule } from '../../auth/auth.module';
import { TransaccionwsController } from './controllers/transaccionws.controller';

@Module({
  imports: [DatabaseModule, CargaMasivaModule, ConfigModule, AuthModule],
  providers: [...TransaccionProvider, TransaccionService],
  exports: [TransaccionService],
  controllers: [TransaccionController, TransaccionwsController],
})
export class TransaccionesModule {
}
