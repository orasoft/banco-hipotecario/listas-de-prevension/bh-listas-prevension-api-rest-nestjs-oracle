import { Inject, Injectable, Logger } from '@nestjs/common';
import { In, Repository } from 'typeorm';
import { CargaMasivaService } from '../../carga-masiva/services/carga.masiva.service';
import { CreacionRegistroRequest } from '../dto/creacion.registro.request';
import { CargaMasiva, Transaccion } from '../../../database/entities';
import { BusquedaRequest } from '../dto/busqueda.request';
import { FiltroBusquedaRequest } from '../dto/filtro.busqueda.request';
import { ConfigService } from '../../../config/config.service';
import { UpdateRequest } from '../dto/update.request';
import { ResultBusquedaDatabase } from '../dto/result.busqueda.database';
import { TransaccionResponse } from '../dto/transaccion.response';

const StringBuilder = require('string-builder');

@Injectable()
export class TransaccionService {
  constructor(@Inject('TRANSACCIONES_REPOSITORY') private readonly repositorio: Repository<Transaccion>,
              private readonly cargaMasivaService: CargaMasivaService,
              private readonly configService: ConfigService) {
  }

  async getSubQueryPerfil(perfilId: string): Promise<string> {
    const query = new StringBuilder();
    // tslint:disable-next-line:max-line-length
    query.appendFormat(`select "camposId" from {0}."campos_perfiles" where "perfilesId" = '{1}'`, this.configService.getConfigDatabase().SCHEMA, perfilId);
    return query.toString();
  }

  async create(request: CreacionRegistroRequest) {
    const cargaMasiva = await this.cargaMasivaService.findById(request.cargaMasivaId);
    const registro = new Transaccion();
    registro.cargaMasiva = cargaMasiva;
    registro.createdBy = request.createdBy;

    return await this.repositorio.manager.save(registro);
  }

  async findById(id: string) {
    return await this.repositorio.findOneOrFail(id);
  }

  async findByIdWithCache(id: string) {
    return await this.repositorio.findOneOrFail(id, { cache: true });
  }

  async findByLista(listaId: string) {
    const queryTransacciones = new StringBuilder();
    // tslint:disable-next-line:max-line-length
    queryTransacciones.appendFormat(`select t."id" from {0}."transacciones" t inner join {0}."cargas_masivas" cm on t."cargaMasivaId" = cm."id" and cm."estado" = 'A' inner join {0}."listas" l on cm."listaId" = l."id" where l."id" = '{1}'`, this.configService.getConfigDatabase().SCHEMA, listaId);
    // tslint:disable-next-line:max-line-length
    const transacciones = await this.repositorio.query(queryTransacciones.toString());
    const transaccionesSelected = [];
    await transacciones.forEach(a => transaccionesSelected.push(a.id));

    if (transaccionesSelected.length > 0) {
      const queryUltimaCarga = new StringBuilder();
      // tslint:disable-next-line:max-line-length
      queryUltimaCarga.appendFormat(`select cm."updatedAt" as "ultimaCarga" from {0}."cargas_masivas" cm where cm."listaId" = '{1}' order by cm."updatedAt" FETCH NEXT 1 ROWS ONLY`, this.configService.getConfigDatabase().SCHEMA, listaId);
      // tslint:disable-next-line:max-line-length
      const ultimaCarga = await this.repositorio.query(queryUltimaCarga.toString());

      const queryInfo = new StringBuilder();
      // tslint:disable-next-line:max-line-length
      queryInfo.appendFormat(`select c2."id" as "campoId", c2."label", lcc."orden" from {0}."listas_campos_config" lcc inner join {0}."campos" c2 on lcc."campoId" = c2."id" inner join {0}."listas" l on lcc."listaId" = l."id" where l."id" in ('{1}') order by lcc."orden"`, this.configService.getConfigDatabase().SCHEMA, listaId);
      // tslint:disable-next-line:max-line-length
      const info = await this.repositorio.query(queryInfo.toString());
      let resultado: any;
      resultado = await this.repositorio.find({
        relations: ['cargaMasiva', 'cargaMasiva.lista', 'detalle', 'detalle.campo'],
        where: {
          id: In(transaccionesSelected),
        },
      });

      return {
        header: info,
        ultimaCarga: ultimaCarga[0].ultimaCarga,
        total: resultado.length,
        filas: resultado,
      };
    } else {
      return {
        ultimaCarga: null,
        header: [],
        total: 0,
        filas: [],
      };
    }
  }

  async findByLastCarga(listaId: string) {
    const queryTransacciones = new StringBuilder();
    // tslint:disable-next-line:max-line-length
    queryTransacciones.appendFormat(`select t."id" from {0}."transacciones" t where "cargaMasivaId" in (select cm."id" from {0}."cargas_masivas" cm where cm."listaId" = '{1}' order by cm."updatedAt" DESC FETCH NEXT 1 ROWS ONLY)`, this.configService.getConfigDatabase().SCHEMA, listaId);
    // tslint:disable-next-line:max-line-length
    const transacciones = await this.repositorio.query(queryTransacciones.toString());
    const transaccionesSelected = [];
    await transacciones.forEach(a => transaccionesSelected.push(a.id));

    if (transaccionesSelected.length > 0) {
      const queryInfo = new StringBuilder();
      // tslint:disable-next-line:max-line-length
      queryInfo.appendFormat(`select c2."id" as "campoId", c2."label", lcc."orden" from {0}."listas_campos_config" lcc inner join {0}."campos" c2 on lcc."campoId" = c2."id" inner join {0}."listas" l on lcc."listaId" = l."id" where l."id" in ('{1}') order by lcc."orden"`, this.configService.getConfigDatabase().SCHEMA, listaId);
      // tslint:disable-next-line:max-line-length
      const info = await this.repositorio.query(queryInfo.toString());

      const ultimaCargaQuery = new StringBuilder();
      // tslint:disable-next-line:max-line-length
      ultimaCargaQuery.appendFormat(`select cm."id", cm."motivo", a2."nombre" as "area", u."usuario" as "cargadoPor", cm."updatedAt" as "fechaCarga" from {0}."cargas_masivas" cm inner join {0}."areas" a2 on cm."areaId" = a2."id" inner join {0}."usuarios" u on cm."usuarioId" = u."id" where cm."listaId" = '{1}' order by cm."updatedAt" DESC FETCH NEXT 1 ROWS ONLY`, this.configService.getConfigDatabase().SCHEMA, listaId);
      // tslint:disable-next-line:max-line-length
      const ultimaCarga = await this.repositorio.query(ultimaCargaQuery.toString());

      let resultado: any;
      resultado = await this.repositorio.find({
        relations: ['detalle', 'detalle.campo'],
        where: {
          id: In(transaccionesSelected),
        },
      });

      return {
        header: info,
        motivo: ultimaCarga[0].motivo,
        solitadoPor: ultimaCarga[0].area,
        cargadoPor: ultimaCarga[0].cargadoPor,
        fechaCarga: ultimaCarga[0].fechaCarga,
        total: resultado.length,
        filas: resultado,
      };
    } else {
      return {
        ultimaCarga: [],
        header: [],
        motivo: '',
        solitadoPor: '',
        cargadoPor: '',
        fechaCarga: '',
        total: 0,
        filas: [],
      };
    }
  }

  async findByCarga(carga: CargaMasiva) {
    return this.repositorio.find({ where: { cargaMasiva: carga }, relations: ['detalle', 'detalle.campo'] });
  }

  async findSystem(request: BusquedaRequest) {
    const perfilId = request.perfilId;

    /* Busquedas exactas */
    const filtros = await this.getFiltros(request.filtros, perfilId);
    const queryBusquedaExacta = new StringBuilder();
    queryBusquedaExacta.appendFormat(`select "id" from ( {0} )`, filtros.join(' intersect '));

    const filtrosLike = await this.getFiltrosLike(request.filtros, perfilId);
    const queryBusquedaLike = new StringBuilder();
    queryBusquedaLike.appendFormat(`select "id" from ( {0} )`, filtrosLike.join(' intersect '));

    const transacciones: string[] = [];

    const queryBusquedaCombinada = [];
    queryBusquedaCombinada.push(queryBusquedaLike.toString());
    queryBusquedaCombinada.push(queryBusquedaExacta.toString());

    const queryFinal = `select "id" from ( ${queryBusquedaCombinada.join(' union ')} )`;
    const camposSeleccionados = await this.repositorio.query(queryFinal);
    await camposSeleccionados.forEach(a => transacciones.push(`'${a.id}'`));

    let resultado: ResultBusquedaDatabase[];
    let info: any;
    if (transacciones.length > 0) {
      const queryResultado = `SELECT TRXS."id" "TRXID",
        LT."id" "LTID",
        LT."nombre" "LTNOMBRE",
        TRXSD."id" "TRXDID",
        (select "U"."usuario" from MIS."listas_responsables" LR
            inner join MIS."usuarios" U on LR."usuariosId" = U."id" where LR."listasId" = LT."id" FETCH FIRST 1 ROWS ONLY) "RESPONSABLE",
        CM."forma" "CMFORMA",
        TRXSD."campoId" "CAMPOID",
        TRXSD."valor" "VALOR"
 FROM MIS."transacciones" "TRXS"
         LEFT JOIN MIS."transacciones_detalle" "TRXSD" ON TRXS."id" = TRXSD."transaccionId"
          LEFT JOIN MIS."campos" "CAMPOS" ON CAMPOS."id" = TRXSD."campoId"
          LEFT JOIN MIS."cargas_masivas" "CM" ON CM."id" = TRXS."cargaMasivaId"
         LEFT JOIN MIS."listas" "LT" ON LT."id" = CM."listaId"
WHERE TRXS."id" IN (${transacciones.join(',')}) ORDER BY TRXS."id"`;

      resultado = await this.repositorio.query(queryResultado);

      let transaccion: TransaccionResponse = new TransaccionResponse();
      const transaccionesResponse: TransaccionResponse[] = [];

      resultado.forEach(a => {
        if (transaccion.id !== a.TRXID) {
          if (transaccion.id !== undefined) {
            transaccionesResponse.push(transaccion);
            transaccion = new TransaccionResponse();
          }
        }
        // Logger.error(`Id => ${a.TRXID}`)
        transaccion.id = a.TRXID;
        transaccion.listaId = a.LTID;
        transaccion.responsable = a.RESPONSABLE;
        transaccion.lista = a.LTNOMBRE;
        transaccion.tipo = a.CMFORMA;
        if (!transaccion.busquedaExacta) {
          transaccion.busquedaExacta = request.filtros[0].valor === a.VALOR;
        }
        transaccion.campos.push({ id: a.CAMPOID, valor: a.VALOR });
      });
      if (resultado.length > 0) {
        transaccionesResponse.push(transaccion);
      }

      if (request.perfilId !== '*') {
        const queryInfo = new StringBuilder();
        // tslint:disable-next-line:max-line-length
        queryInfo.appendFormat(` select c2."id" as "campoId", c2."label", lcc."orden" from {0}."listas_campos_config" lcc inner join {0}."campos" c2 on lcc."campoId" = c2."id" inner join {0}."listas" l on lcc."listaId" = l."id" where lcc."campoId" in ( select "camposId" from {0}."campos_perfiles" where "perfilesId" = '{1}' ) and l."id" in ( select {0}."perfiles"."listaId" from {0}."perfiles" where "id" = '{1}' ) order by lcc."orden"`, this.configService.getConfigDatabase().SCHEMA, perfilId);
        info = await this.repositorio.query(queryInfo.toString());
      } else {
        const queryInfo = new StringBuilder();
        // tslint:disable-next-line:max-line-length
        queryInfo.appendFormat(`select rownum as "orden", "campoId", "label" from ( SELECT "c"."id" AS "campoId", "c"."label" AS "label" FROM {0}."campos" "c" WHERE "c"."default" = 1 order by "c"."createdAt")`, this.configService.getConfigDatabase().SCHEMA);
        info = await this.repositorio.query(queryInfo.toString());
      }
      return {
        header: info,
        total: transaccionesResponse.length,
        filas: transaccionesResponse,
      };
    } else {
      return {
        header: [],
        total: 0,
        fila: [],
      };
    }

  }

  async deleteById(id: string, mensaje: UpdateRequest) {
    const record = await this.repositorio.findOneOrFail(id);
    record.updatedBy = mensaje.updateBy;
    record.justificacion = mensaje.justificacion;
    record.estado = 'N';
    this.repositorio.manager.save(record);
  }

  private async getFiltros(filtros: FiltroBusquedaRequest[], perfilId: string): Promise<any[]> {
    const filtrosArray = [];
    await filtros.forEach(async campo => {
      const cadena = new StringBuilder();
      // tslint:disable-next-line:max-line-length
      cadena.appendFormat(`select t."id" from {0}."transacciones" t inner join {0}."cargas_masivas" cm on t."cargaMasivaId" = cm."id" and cm."estado" = 'A' inner join {0}."transacciones_detalle" td on t."id" = td."transaccionId" where (LOWER(td."valor") = '{1}' and td."campoId" = '{2}')`, this.configService.getConfigDatabase().SCHEMA, campo.valor.toLowerCase(), campo.campoId);

      if (perfilId.trim() !== '*') {
        const camposIn = await this.getSubQueryPerfil(perfilId);
        cadena.appendFormat('and td."campoId" in ( {0} ) ', camposIn);
        cadena.appendFormat(`and cm."listaId" = (select x."listaId" from MIS."perfiles" x where x."id" = '{0}')`, perfilId);
      }

      filtrosArray.push(cadena.toString());
    });
    return filtrosArray;
  }

  private async getFiltrosLike(filtros: FiltroBusquedaRequest[], perfilId: string): Promise<any[]> {
    const filtrosArray = [];
    await filtros.forEach(async campo => {
      const cadena = new StringBuilder();
      // tslint:disable-next-line:max-line-length
      cadena.appendFormat(`select t."id" from {0}."transacciones" t inner join {0}."cargas_masivas" cm on t."cargaMasivaId" = cm."id" and cm."estado" = 'A' inner join {0}."transacciones_detalle" td on t."id" = td."transaccionId" where (LOWER(td."valor") like '%{1}%' and td."campoId" = '{2}')`, this.configService.getConfigDatabase().SCHEMA, campo.valor.toLowerCase(), campo.campoId);

      if (perfilId.trim() !== '*') {
        const camposIn = await this.getSubQueryPerfil(perfilId);
        cadena.appendFormat('and td."campoId" in ( {0} ) ', camposIn);
        cadena.appendFormat(`and cm."listaId" = (select x."listaId" from MIS."perfiles" x where x."id" = '{0}')`, perfilId);
      }

      filtrosArray.push(cadena.toString());
    });

    return filtrosArray;
  }
}
