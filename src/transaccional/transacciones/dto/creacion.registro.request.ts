import { ApiModelProperty } from '@nestjs/swagger';

export class CreacionRegistroRequest {

  @ApiModelProperty()
  cargaMasivaId: string;

  @ApiModelProperty()
  createdBy?: string;
}
