import { FiltroBusquedaRequest } from './filtro.busqueda.request';

export class BusquedaRequest {
  perfilId: string;
  filtros: FiltroBusquedaRequest[];
}
