export class ResultBusquedaDatabase {
  TRXID: string;
  LTID: string;
  LTNOMBRE: string;
  TRXDID: string;
  RESPONSABLE: string;
  CMFORMA: string;
  CAMPOID: string;
  VALOR: string;
}
