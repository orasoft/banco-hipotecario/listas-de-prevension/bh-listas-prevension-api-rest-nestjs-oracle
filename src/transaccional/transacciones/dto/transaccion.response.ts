import { CampoResponse } from './campo.response';

export class TransaccionResponse {
  id: string;
  listaId: string;
  lista: string;
  responsable: string;
  busquedaExacta: boolean = false;
  tipo: string;
  campos: CampoResponse[] = [];
}
