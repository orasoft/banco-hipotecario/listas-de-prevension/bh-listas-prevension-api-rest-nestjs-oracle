import { ApiUseTags } from '@nestjs/swagger';
import { Body, Controller, Get, Param, Post, Put, Request, UseGuards, Delete, Logger } from '@nestjs/common';
import { TransaccionService } from '../services/transaccion.service';
import { CreacionRegistroRequest } from '../dto/creacion.registro.request';
import { BusquedaRequest } from '../dto/busqueda.request';
import { AuthGuard } from '@nestjs/passport';
import { UsuarioStrategy } from '../../../auth/dtos/usuario.strategy';
import { UpdateRequest } from '../dto/update.request';
import { identity } from 'rxjs';

@ApiUseTags('Transacciones')
@UseGuards(AuthGuard('jwt'))
@Controller('/v1/transaccional/transacciones')
export class TransaccionController {

  constructor(private readonly service: TransaccionService) {
  }

  @Get('/consulta/:lista')
  async transacciones(@Param('lista') listaId: string) {
    return await this.service.findByLista(listaId);
  }

  @Get('/ultimacarga/:lista')
  async transaccionesUltimaCarga(@Param('lista') listaId: string) {
    return await this.service.findByLastCarga(listaId);
  }

  @Post('consulta')
  async findTransactionsWithFilters(@Body() request: BusquedaRequest) {
    return await this.service.findSystem(request);
  }

  @Post()
  async create(@Request() request, @Body() mensaje: CreacionRegistroRequest) {
    const tokenInfo: UsuarioStrategy = request.user;
    Logger.log(`tokeninfo ====> ${JSON.stringify(tokenInfo)}`)
    mensaje.createdBy = tokenInfo.usuario.codbh;
    return await this.service.create(mensaje);
  }

  // TODO - Actualizar un registro en especifico, en el body vendra la lista de columnas actualizadas.
  @Delete(':id')
  async update(@Param('id') id, @Request() request, @Body() mensaje: UpdateRequest) {
    const tokenInfo: UsuarioStrategy = request.user;
    mensaje.updateBy = tokenInfo.usuario.codbh;
    return this.service.deleteById(id, mensaje);
  }
}
