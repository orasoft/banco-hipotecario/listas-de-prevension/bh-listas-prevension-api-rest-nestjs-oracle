import { Body, Controller, Post } from '@nestjs/common';
import { TransaccionService } from '../services/transaccion.service';
import { BusquedaRequest } from '../dto/busqueda.request';

@Controller('v1/webservice')
export class TransaccionwsController {

  constructor(private readonly service: TransaccionService) {
  }

  @Post('filter')
  async findTransactionsWithFilters(@Body() request: BusquedaRequest) {
    return await this.service.findSystem(request);
  }

}
