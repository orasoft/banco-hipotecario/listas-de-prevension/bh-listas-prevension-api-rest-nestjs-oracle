import { ConfigService } from '../config/config.service';
import { Logger } from '@nestjs/common';
import { schedule } from 'node-cron';
import { CreacionRolRequest } from '../manager/rol/dto/creacion.rol.request';

const axios = require('axios');
const soap = require('soap');
const parseString = require('xml2js').parseString;

export class RolesBatch {
  static sincronizar() {
    const config = new ConfigService(`.env`);
    const seguridadUrl = config.getUrlSegService();
    const args = {
      codSistema: 'LSTCT',
    };

    const task = schedule('* * * * *', () => {
      soap.createClient(seguridadUrl, (err, client) => {
        try {
          if (client !== undefined) {
            client.perfilesAplicacion(args, (err2, response) => {
              parseString(response.return, async (err3, result) => {

                Logger.verbose(`result Roles => ${JSON.stringify(result)}`);


                if (result.perfilesAplicacion.exitoso.toString() === '0') {
                  const usuarios = result.perfilesAplicacion.perfiles[0].perfil;
                  usuarios.forEach(a => {
                    const record = new CreacionRolRequest();
                    record.nombre = a.nombre[0];
                    record.idBhRol = a.id[0];

                    Logger.log(`Record => ${JSON.stringify(record)}`);
                    const urlApi = config.getUrlApi();

                    // tslint:disable-next-line:no-shadowed-variable
                    axios.post(`${urlApi}/config/roles/sincronizacion`, record).then(() => {
                      Logger.log(`[ EXITO ] Sincronizacion de Roles.`);
                    }).catch((error) => {
                      Logger.error('[ ERROR ] No fue posible sincronizar los Roles');
                      Logger.error('[ ERROR ] ' + JSON.stringify(error) );
                    });
                  });
                }
              });
            });
          }
        } catch (e) {
          Logger.error(JSON.stringify(e));
        }
      });
    });
    return task;
  }
}
