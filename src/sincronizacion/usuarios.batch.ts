import { ConfigService } from '../config/config.service';
import { Logger } from '@nestjs/common';
import { schedule } from 'node-cron';
import { CreacionUsuarioRequest } from '../manager/usuario/dto/creacion.usuario.request';

const axios = require('axios');
const soap = require('soap');
const parseString = require('xml2js').parseString;

export class UsuariosBatch {
  static sincronizar() {
    const config = new ConfigService(`.env`);
    const seguridadUrl = config.getUrlSegService();
    const args = {
      codSistema: 'LSTCT',
    };

    const task = schedule('* * * * *', () => {
      soap.createClient(seguridadUrl, (err, client) => {
        try {
          if (client !== undefined) {
            client.usuariosAplicacion(args, (err2, response) => {
              parseString(response.return, async (err3, result) => {
                if (result.usuariosAplicacion.exitoso.toString() === '0') {
                  const usuarios = result.usuariosAplicacion.usuarios[0].usuario;
                  usuarios.forEach(a => {
                    const record = new CreacionUsuarioRequest();
                    record.usuario = a.nombre[0];
                    record.codbh = a.codigoBH[0];

                    Logger.log(`Record => ${JSON.stringify(record)}`);
                    const urlApi = config.getUrlApi();

                    // tslint:disable-next-line:no-shadowed-variable
                    axios.post(`${urlApi}/config/usuarios/sincronizacion`, record).then(() => {
                      Logger.log(`[ EXITO ] Sincronizacion de usuarios.`);
                    }).catch(() => {
                      Logger.error('[ ERROR ] No fue posible sincronizar los Usuarios');
                    });
                  });
                }
              });
            });
          }
        } catch (e) {
          Logger.error(JSON.stringify(e));
        }
      });
    });
    return task;
  }
}
