import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { HttpExceptionFilter } from './http.exception.filter';
import { Logger } from '@nestjs/common';
import { schedule } from 'node-cron';
import { CreacionUsuarioRequest } from './manager/usuario/dto/creacion.usuario.request';
import { UsuariosBatch } from './sincronizacion/usuarios.batch';
import { RolesBatch } from './sincronizacion/roles.batch';

const axios = require('axios');
const soap = require('soap');
const parseString = require('xml2js').parseString;

async function sincronizacionUsuarios() {
  const task = UsuariosBatch.sincronizar();
  task.start();
}

async function sincronizacionRoles() {
  const task = RolesBatch.sincronizar();
  task.start();
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const globalPrefix = 'api';
  app.enableCors();
  app.useGlobalFilters(new HttpExceptionFilter());
  const options = new DocumentBuilder()
    .setTitle('Banco Hipotecario')
    .setDescription('Listas para monitoreo')
    .setVersion('1.0')
    .setBasePath(`/${globalPrefix}`)
    .setContactEmail('rex2002xp@gmail.com')
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('doc', app, document);

  app.setGlobalPrefix(globalPrefix);
  await app.listen(3000);
}

sincronizacionUsuarios();
sincronizacionRoles();
bootstrap();
