import { TerminusModule } from '@nestjs/terminus';
import { TerminusOptionsService } from './terminus-options.service';
import { Module } from '@nestjs/common';
import { DatabaseModule } from './database/database.module';
import { AppController } from './app.controller';
import { ConfigCampoModule } from './manager/campo/config.campo.module';
import { ListaModule } from './manager/lista/lista.module';
import { ListaConfigModule } from './manager/lista_config/lista.config.module';
import { UsuarioModule } from './manager/usuario/usuario.module';
import { RolesModule } from './manager/rol/roles.module';
import { PerfilesModule } from './manager/perfil/perfiles.module';
import { ConexionExternaModule } from './manager/conexion_externa/conexion.externa.module';
import { AreaModule } from './manager/area/area.module';
import { CargaMasivaModule } from './transaccional/carga-masiva/carga.masiva.module';
import { TransaccionesModule } from './transaccional/transacciones/transacciones.module';
import { ColumnasModule } from './transaccional/columnas/columnas.module';
import { ConfigModule } from './config/config.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    TerminusModule.forRootAsync({
      useClass: TerminusOptionsService,
    }),
    DatabaseModule,
    ConfigCampoModule,
    ListaModule,
    ListaConfigModule,
    UsuarioModule,
    RolesModule,
    PerfilesModule,
    AreaModule,
    ConexionExternaModule,
    CargaMasivaModule,
    TransaccionesModule,
    ColumnasModule,
    ConfigModule,
    AuthModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {
}
