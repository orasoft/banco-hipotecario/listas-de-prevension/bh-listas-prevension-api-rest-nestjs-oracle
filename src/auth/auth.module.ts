import { ConfigModule } from '../config/config.module';
import { UsuarioModule } from '../manager/usuario/usuario.module';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { AuthExternaService } from './service/auth.externa.service';
import { AuthController } from './constrollers/auth.controller';
import { AuthTokenService } from './service/auth.token.service';
import { JwtStrategy } from './service/jwt.strategy';
import { Module } from '@nestjs/common';
import { MeController } from './constrollers/me.controller';
import { AuthwsController } from './constrollers/authws.controller';

@Module({
  imports: [
    ConfigModule,
    UsuarioModule,
    PassportModule,
    JwtModule.register({
      secret: '1234567890',
    }),
  ],
  controllers: [AuthController, MeController, AuthwsController],
  providers: [AuthExternaService, AuthTokenService, JwtStrategy],
})
export class AuthModule {
}
