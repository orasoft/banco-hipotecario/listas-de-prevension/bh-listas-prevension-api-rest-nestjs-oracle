import { Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { UsuariosService } from '../../manager/usuario/services/usuarios.service';
import { LoginRequest } from '../dtos/login.request';
import { AuthExternaService } from './auth.externa.service';
import { InfoUserModel } from '../dtos/info.user.model';
import { Usuario } from '../../database/entities';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from '../dtos/jwtpayload';

@Injectable()
export class AuthTokenService {

  private infoUser: InfoUserModel;

  constructor(private readonly usuarioService: UsuariosService,
              private readonly authExterna: AuthExternaService,
              private readonly jwtService: JwtService) {
  }

  async createToken(request: LoginRequest) {
    const userExist: Usuario = await this.validateUser(request);
    if (userExist) {
      const payload: JwtPayload = { codbh: userExist.codbh, id: userExist.id, roles: this.infoUser.roles };
      const accessToken = this.jwtService.sign(payload, { expiresIn: 3600, audience: 'bhipotecario-authentication' });
      const refreshToken = this.jwtService.sign(payload, { expiresIn: 7200, audience: 'bhipotecario-refresh' });
      const expiresIn = 7200;

      return { expiresIn, accessToken, refreshToken };
    }
    return userExist;
  }

  async createTokenWs(request: LoginRequest) {
    const userExist: Usuario = await this.validateUser(request);
    if (userExist) {
      const payload: JwtPayload = { codbh: userExist.codbh, id: userExist.id };
      const accessToken = this.jwtService.sign(payload, { expiresIn: 3600, audience: 'bhipotecario-authentication' });
      const refreshToken = this.jwtService.sign(payload, { expiresIn: 7200, audience: 'bhipotecario-refresh' });
      const expiresIn = 3600;

      return { expiresIn, accessToken, refreshToken };
    }
    return userExist;
  }

  async validateUser(request: LoginRequest): Promise<Usuario> {
    this.infoUser = null;
    this.infoUser = await this.authExterna.getInfo(request.username, request.password);
    Logger.log(`infoUser => ${JSON.stringify(this.infoUser)}`);
    if (this.infoUser.status > 0) {
      throw new UnauthorizedException();
    } else {
      return this.getUserApplication(request);
    }
  }

  private getUserApplication(request: LoginRequest): Promise<Usuario> {
    return new Promise<Usuario>(resolve => {
      this.usuarioService.findByBhCode(this.infoUser.information.bhcod).then(record => {
        return resolve(record);
      }).catch(async (error) => {
        Logger.log(`this.infoUser.information => ${JSON.stringify(this.infoUser.information)}`);
        const user = await this.usuarioService.create({
            usuario: this.infoUser.information.user,
            codbh: this.infoUser.information.bhcod,
          }, this.infoUser.information.bhcod);
        return resolve(user);
      });
    });
  }
}
