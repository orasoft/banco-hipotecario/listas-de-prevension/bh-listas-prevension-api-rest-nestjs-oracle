import { PassportStrategy } from '@nestjs/passport';
import { Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { JwtPayload } from '../dtos/jwtpayload';
import { UsuariosService } from '../../manager/usuario/services/usuarios.service';
import { ExtractJwt, Strategy } from 'passport-jwt';
import { UsuarioStrategy } from '../dtos/usuario.strategy';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly usuarioService: UsuariosService) {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: '1234567890',
    });
  }

  async validate(payload: JwtPayload) {
    const infoToken = new UsuarioStrategy();
    const user = await this.usuarioService.findByBhCode(payload.codbh);
    if (!user) {
      throw new UnauthorizedException();
    }
    const roles = [];
    Logger.log(payload)
    if (payload.roles) {
      payload.roles.forEach(a => {
        roles.push({ id: a.id, rol: a.name });
      });
    }
    infoToken.usuario = user;
    infoToken.roles = roles;
    infoToken.accesos = payload.roles;
    return infoToken;
  }
}
