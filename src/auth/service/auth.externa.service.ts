import { Injectable, Logger, UnauthorizedException } from '@nestjs/common';
import { ConfigService } from '../../config/config.service';
import { RolModel } from '../dtos/rol.model';
import { AccessItemModel } from '../dtos/access.item.model';
import { AccessParametersModel } from '../dtos/access.parameters.model';
import { InfoUserModel } from '../dtos/info.user.model';

const soap = require('soap');
const parseString = require('xml2js').parseString;

@Injectable()
export class AuthExternaService {
    private url: string;
    private args: AccessParametersModel;
    private infoUser: InfoUserModel;

    constructor(private readonly configService: ConfigService) {
        this.url = this.configService.getUrlSegService();
        this.args = {
            usuario: '',
            contrasenia: '',
            sistema: 'LSTCT',
            tipoSesion: 'LDAP',
        };
        this.infoUser = new InfoUserModel();
    }

    async getInfo(usernane: string, password: string): Promise<InfoUserModel> {

        this.infoUser = new InfoUserModel();

        this.args.usuario = usernane;
        this.args.contrasenia = password;

        return this.getClient(this.url)
          .then((client) => {
              return this.getSoapMessage(client);
          })
          .then(messageRaw => {
              return this.parseMessage(messageRaw);
          }).then(infoUser => {
              return infoUser;
          }).catch(err => {
              return err;
          });
    }

    private async getClient(url) {
        return new Promise((resolve, reject) => {
            soap.createClient(url, (err, client) => {
                if (client !== undefined) {
                    return resolve(client);
                } else {
                    return reject(err);
                }
            });
        });
    }

    private getSoapMessage(client) {
        return new Promise((resolve, reject) => {
            client.iniciarSesion(this.args, (err, result) => {
                return resolve(result.return);
            });
        });
    }

    private parseMessage(messageRaw): Promise<InfoUserModel> {
        return new Promise((resolve, reject) => {
            parseString(messageRaw, (error, result) => {
                if (result.peticionSeguridad.exitoso.toString() === '0') {
                    const logged = result.peticionSeguridad.exitoso.toString();
                    const codbh = result.peticionSeguridad.codigoBH.toString();
                    const usuario = result.peticionSeguridad.nombreUsuario.toString();
                    const area = result.peticionSeguridad.area.toString();
                    const sistema = result.peticionSeguridad.sistema.toString();

                    const roles: RolModel[] = [];
                    const accesos: AccessItemModel[] = [];
                    result.peticionSeguridad.privilegios.forEach(data => {
                        data.roles.forEach(data1 => {
                            data1.rol.forEach(data2 => {
                                data2.accesos.forEach(data3 => {
                                    data3.acceso.forEach(data4 => {
                                        const acc: AccessItemModel = {
                                            id: data4.id,
                                            idPrincipal: data4.idPadre,
                                            type: data4.$.type,
                                            name: data4.nombreAcceso,
                                            url: data4.url,
                                            icon: data4.icono,
                                        };
                                        accesos.push(acc);
                                    });
                                });
                                const rol: RolModel = {
                                    name: data2.$.type,
                                    id: data2.$.id,
                                    access: accesos,
                                };
                                roles.push(rol);
                            });
                        });
                    });
                    this.infoUser.information = {
                            login: logged,
                            bhcod: codbh,
                            user: usuario,
                            branch: area,
                            system: sistema,
                        };
                    this.infoUser.roles = roles;
                    this.infoUser.status = 0;
                    return resolve(this.infoUser);
                } else {
                    this.infoUser.status = 1;
                    return reject(this.infoUser);
                }
            });
        });
    }
}
