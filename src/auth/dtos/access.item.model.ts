export interface AccessItemModel {
  id: any[];
  idPrincipal: any[];
  type: string;
  name: any[];
  url: any[];
  icon: any[];
}
