import { Usuario } from '../../database/entities';
import { RolModel } from './rol.model';

export class UsuarioStrategy {
  usuario: Usuario;
  roles: string[];
  accesos: RolModel[];
}
