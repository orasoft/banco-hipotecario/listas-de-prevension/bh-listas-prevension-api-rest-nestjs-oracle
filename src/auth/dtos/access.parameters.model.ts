export interface AccessParametersModel {
  usuario: string;
  contrasenia: string;
  sistema: string;
  tipoSesion: string;
}
