import { RolModel } from './rol.model';

export interface JwtPayload {
  id: string;
  codbh: string;
  roles?: RolModel[];
}
