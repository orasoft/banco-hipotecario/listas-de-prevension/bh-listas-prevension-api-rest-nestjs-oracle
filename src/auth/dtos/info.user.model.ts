import { RolModel } from './rol.model';
import { InfoDetalleModel } from './info.detalle.model';

export class InfoUserModel {
  information: InfoDetalleModel;
  roles: RolModel[];
  status: number;

  constructor() {
    this.roles = [];
    this.information = new InfoDetalleModel();
    this.status = 1;
  }
}
