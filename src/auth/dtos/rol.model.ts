import { AccessItemModel } from './access.item.model';

export interface RolModel {
  id: string;
  name: string;
  access: AccessItemModel[];
}
