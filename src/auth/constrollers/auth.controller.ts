import { Body, Controller, Get, Request, Post, UseGuards } from '@nestjs/common';
import { LoginRequest } from '../dtos/login.request';
import { AuthTokenService } from '../service/auth.token.service';
import { AuthGuard } from '@nestjs/passport';

@Controller('v1/auth')
export class AuthController {
  constructor(private readonly authTokenService: AuthTokenService) {
  }

  @Post()
  async login(@Body() request: LoginRequest) {
    return this.authTokenService.createToken(request).then(token => {
      return token;
    }).catch( err => {
      return err;
    });
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('me')
  async me(@Request() request) {
    return request.user;
  }
}
