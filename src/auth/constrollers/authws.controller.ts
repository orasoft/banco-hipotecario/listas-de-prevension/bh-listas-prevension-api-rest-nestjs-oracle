import { Body, Controller, Post } from '@nestjs/common';
import { LoginRequest } from '../dtos/login.request';
import { AuthTokenService } from '../service/auth.token.service';

@Controller('v1/webservice')
export class AuthwsController {

  constructor(private readonly authTokenService: AuthTokenService) {
  }

  @Post('auth')
  async loginWs(@Body() request: LoginRequest) {
    return this.authTokenService.createTokenWs(request).then(token => {
      return token;
    }).catch(err => {
      return err;
    });
  }
}
