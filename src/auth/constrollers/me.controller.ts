import { Controller, Get, Logger, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

@UseGuards(AuthGuard('jwt'))
@Controller('v1/session')
export class MeController {
  @Get('me')
  async me(@Request() request) {
    return request.user;
  }
}
